<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/bookmarks/grid', 'BookmarksController@grid');
Route::resource('/bookmarks', 'BookmarksController');
Route::get('/cms_apicustoms/grid', 'Cms_apicustomsController@grid');
Route::resource('/cms_apicustoms', 'Cms_apicustomsController');
Route::get('/cms_apikeys/grid', 'Cms_apikeysController@grid');
Route::resource('/cms_apikeys', 'Cms_apikeysController');
Route::get('/cms_dashboards/grid', 'Cms_dashboardsController@grid');
Route::resource('/cms_dashboards', 'Cms_dashboardsController');
Route::get('/cms_email_queues/grid', 'Cms_email_queuesController@grid');
Route::resource('/cms_email_queues', 'Cms_email_queuesController');
Route::get('/cms_email_templates/grid', 'Cms_email_templatesController@grid');
Route::resource('/cms_email_templates', 'Cms_email_templatesController');
Route::get('/cms_logs/grid', 'Cms_logsController@grid');
Route::resource('/cms_logs', 'Cms_logsController');
Route::get('/cms_menus/grid', 'Cms_menusController@grid');
Route::resource('/cms_menus', 'Cms_menusController');
Route::get('/cms_moduls/grid', 'Cms_modulsController@grid');
Route::resource('/cms_moduls', 'Cms_modulsController');
Route::get('/cms_notifications/grid', 'Cms_notificationsController@grid');
Route::resource('/cms_notifications', 'Cms_notificationsController');
Route::get('/cms_privileges/grid', 'Cms_privilegesController@grid');
Route::resource('/cms_privileges', 'Cms_privilegesController');
Route::get('/cms_privileges_roles/grid', 'Cms_privileges_rolesController@grid');
Route::resource('/cms_privileges_roles', 'Cms_privileges_rolesController');
Route::get('/cms_settings/grid', 'Cms_settingsController@grid');
Route::resource('/cms_settings', 'Cms_settingsController');
Route::get('/cms_statistic_components/grid', 'Cms_statistic_componentsController@grid');
Route::resource('/cms_statistic_components', 'Cms_statistic_componentsController');
Route::get('/cms_statistics/grid', 'Cms_statisticsController@grid');
Route::resource('/cms_statistics', 'Cms_statisticsController');
Route::get('/cms_users/grid', 'Cms_usersController@grid');
Route::resource('/cms_users', 'Cms_usersController');
Route::get('/dana_sosials/grid', 'Dana_sosialsController@grid');
Route::resource('/dana_sosials', 'Dana_sosialsController');
Route::get('/dana_sosial_details/grid', 'Dana_sosial_detailsController@grid');
Route::resource('/dana_sosial_details', 'Dana_sosial_detailsController');
Route::get('/gerejas/grid', 'GerejasController@grid');
Route::resource('/gerejas', 'GerejasController');
Route::get('/jadwal_kegiatans/grid', 'Jadwal_kegiatansController@grid');
Route::resource('/jadwal_kegiatans', 'Jadwal_kegiatansController');
Route::get('/jenis_ibadahs/grid', 'Jenis_ibadahsController@grid');
Route::resource('/jenis_ibadahs', 'Jenis_ibadahsController');
Route::get('/kategori_danasosials/grid', 'Kategori_danasosialsController@grid');
Route::resource('/kategori_danasosials', 'Kategori_danasosialsController');
Route::get('/kategori_gerejas/grid', 'Kategori_gerejasController@grid');
Route::resource('/kategori_gerejas', 'Kategori_gerejasController');
Route::get('/kidung_pujians/grid', 'Kidung_pujiansController@grid');
Route::resource('/kidung_pujians', 'Kidung_pujiansController');
Route::get('/kitabs/grid', 'KitabsController@grid');
Route::resource('/kitabs', 'KitabsController');
Route::get('/kitab_pujian_kategoris/grid', 'Kitab_pujian_kategorisController@grid');
Route::resource('/kitab_pujian_kategoris', 'Kitab_pujian_kategorisController');
Route::get('/konten_ibadahs/grid', 'Konten_ibadahsController@grid');
Route::resource('/konten_ibadahs', 'Konten_ibadahsController');
Route::get('/migrations/grid', 'MigrationsController@grid');
Route::resource('/migrations', 'MigrationsController');
Route::get('/pendeta/grid', 'PendetaController@grid');
Route::resource('/pendeta', 'PendetaController');
Route::get('/posts/grid', 'PostsController@grid');
Route::resource('/posts', 'PostsController');
Route::get('/profiles/grid', 'ProfilesController@grid');
Route::resource('/profiles', 'ProfilesController');
Route::get('/renungan_harians/grid', 'Renungan_hariansController@grid');
Route::resource('/renungan_harians', 'Renungan_hariansController');
Route::get('/roles/grid', 'RolesController@grid');
Route::resource('/roles', 'RolesController');
Route::get('/tata_ibadahs/grid', 'Tata_ibadahsController@grid');
Route::resource('/tata_ibadahs', 'Tata_ibadahsController');
Route::get('/users/grid', 'UsersController@grid');
Route::resource('/users', 'UsersController');
Route::get('/versi_kitab_pujians/grid', 'Versi_kitab_pujiansController@grid');
Route::resource('/versi_kitab_pujians', 'Versi_kitab_pujiansController');