<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Gereja;

use DB;

class GerejasController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('gerejas.index', []);
	}

	public function create(Request $request)
	{
	    return view('gerejas.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$gereja = Gereja::findOrFail($id);
	    return view('gerejas.add', [
	        'model' => $gereja	    ]);
	}

	public function show(Request $request, $id)
	{
		$gereja = Gereja::findOrFail($id);
	    return view('gerejas.show', [
	        'model' => $gereja	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM gereja a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_gereja LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$gereja = null;
		if($request->id > 0) { $gereja = Gereja::findOrFail($request->id); }
		else { 
			$gereja = new Gereja;
		}
	    

	    		
					    $gereja->gereja_id = $request->gereja_id;
		
	    		
					    $gereja->nama_gereja = $request->nama_gereja;
		
	    		
					    $gereja->foto_gereja = $request->foto_gereja;
		
	    		
					    $gereja->sejarah_gereja = $request->sejarah_gereja;
		
	    		
					    $gereja->alamat = $request->alamat;
		
	    		
					    $gereja->latitude = $request->latitude;
		
	    		
					    $gereja->langitude = $request->langitude;
		
	    		
					    $gereja->status = $request->status;
		
	    		
					    $gereja->created_by = $request->created_by;
		
	    		
					    $gereja->updated_at = $request->updated_at;
		
	    		
					    $gereja->kategorigereja_id = $request->kategorigereja_id;
		
	    		
					    $gereja->user_id = $request->user_id;
		
	    	    //$gereja->user_id = $request->user()->id;
	    $gereja->save();

	    return redirect('/gerejas');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$gereja = Gereja::findOrFail($id);

		$gereja->delete();
		return "OK";
	    
	}

	
}