<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Dana_sosial;

use DB;

class Dana_sosialsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('dana_sosials.index', []);
	}

	public function create(Request $request)
	{
	    return view('dana_sosials.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$dana_sosial = Dana_sosial::findOrFail($id);
	    return view('dana_sosials.add', [
	        'model' => $dana_sosial	    ]);
	}

	public function show(Request $request, $id)
	{
		$dana_sosial = Dana_sosial::findOrFail($id);
	    return view('dana_sosials.show', [
	        'model' => $dana_sosial	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM dana_sosial a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE judul LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$dana_sosial = null;
		if($request->id > 0) { $dana_sosial = Dana_sosial::findOrFail($request->id); }
		else { 
			$dana_sosial = new Dana_sosial;
		}
	    

	    		
					    $dana_sosial->danasosial_id = $request->danasosial_id;
		
	    		
					    $dana_sosial->judul = $request->judul;
		
	    		
					    $dana_sosial->created_by = $request->created_by;
		
	    		
					    $dana_sosial->target_dana = $request->target_dana;
		
	    		
					    $dana_sosial->deadline = $request->deadline;
		
	    		
					    $dana_sosial->alamat = $request->alamat;
		
	    		
					    $dana_sosial->status_danasosial = $request->status_danasosial;
		
	    		
					    $dana_sosial->status = $request->status;
		
	    		
					    $dana_sosial->kategori_id = $request->kategori_id;
		
	    		
					    $dana_sosial->user_id = $request->user_id;
		
	    		
					    $dana_sosial->detail_id = $request->detail_id;
		
	    	    //$dana_sosial->user_id = $request->user()->id;
	    $dana_sosial->save();

	    return redirect('/dana_sosials');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$dana_sosial = Dana_sosial::findOrFail($id);

		$dana_sosial->delete();
		return "OK";
	    
	}

	
}