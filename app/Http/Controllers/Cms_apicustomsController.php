<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_apicustom;

use DB;

class Cms_apicustomsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_apicustoms.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_apicustoms.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_apicustom = Cms_apicustom::findOrFail($id);
	    return view('cms_apicustoms.add', [
	        'model' => $cms_apicustom	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_apicustom = Cms_apicustom::findOrFail($id);
	    return view('cms_apicustoms.show', [
	        'model' => $cms_apicustom	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_apicustom a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_apicustom = null;
		if($request->id > 0) { $cms_apicustom = Cms_apicustom::findOrFail($request->id); }
		else { 
			$cms_apicustom = new Cms_apicustom;
		}
	    

	    		
			    $cms_apicustom->id = $request->id?:0;
				
	    		
					    $cms_apicustom->created_at = $request->created_at;
		
	    		
					    $cms_apicustom->updated_at = $request->updated_at;
		
	    		
					    $cms_apicustom->permalink = $request->permalink;
		
	    		
					    $cms_apicustom->tabel = $request->tabel;
		
	    		
					    $cms_apicustom->aksi = $request->aksi;
		
	    		
					    $cms_apicustom->kolom = $request->kolom;
		
	    		
					    $cms_apicustom->orderby = $request->orderby;
		
	    		
					    $cms_apicustom->sub_query_1 = $request->sub_query_1;
		
	    		
					    $cms_apicustom->sql_where = $request->sql_where;
		
	    		
					    $cms_apicustom->nama = $request->nama;
		
	    		
					    $cms_apicustom->keterangan = $request->keterangan;
		
	    		
					    $cms_apicustom->parameter = $request->parameter;
		
	    		
					    $cms_apicustom->method_type = $request->method_type;
		
	    		
					    $cms_apicustom->parameters = $request->parameters;
		
	    		
					    $cms_apicustom->responses = $request->responses;
		
	    	    //$cms_apicustom->user_id = $request->user()->id;
	    $cms_apicustom->save();

	    return redirect('/cms_apicustoms');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_apicustom = Cms_apicustom::findOrFail($id);

		$cms_apicustom->delete();
		return "OK";
	    
	}

	
}