<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_statistic_component;

use DB;

class Cms_statistic_componentsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_statistic_components.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_statistic_components.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_statistic_component = Cms_statistic_component::findOrFail($id);
	    return view('cms_statistic_components.add', [
	        'model' => $cms_statistic_component	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_statistic_component = Cms_statistic_component::findOrFail($id);
	    return view('cms_statistic_components.show', [
	        'model' => $cms_statistic_component	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_statistic_components a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE id_cms_statistics LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_statistic_component = null;
		if($request->id > 0) { $cms_statistic_component = Cms_statistic_component::findOrFail($request->id); }
		else { 
			$cms_statistic_component = new Cms_statistic_component;
		}
	    

	    		
			    $cms_statistic_component->id = $request->id?:0;
				
	    		
					    $cms_statistic_component->id_cms_statistics = $request->id_cms_statistics;
		
	    		
					    $cms_statistic_component->componentID = $request->componentID;
		
	    		
					    $cms_statistic_component->component_name = $request->component_name;
		
	    		
					    $cms_statistic_component->area_name = $request->area_name;
		
	    		
					    $cms_statistic_component->sorting = $request->sorting;
		
	    		
					    $cms_statistic_component->name = $request->name;
		
	    		
					    $cms_statistic_component->config = $request->config;
		
	    		
					    $cms_statistic_component->created_at = $request->created_at;
		
	    		
					    $cms_statistic_component->updated_at = $request->updated_at;
		
	    	    //$cms_statistic_component->user_id = $request->user()->id;
	    $cms_statistic_component->save();

	    return redirect('/cms_statistic_components');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_statistic_component = Cms_statistic_component::findOrFail($id);

		$cms_statistic_component->delete();
		return "OK";
	    
	}

	
}