<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_menu;

use DB;

class Cms_menusController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_menus.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_menus.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_menu = Cms_menu::findOrFail($id);
	    return view('cms_menus.add', [
	        'model' => $cms_menu	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_menu = Cms_menu::findOrFail($id);
	    return view('cms_menus.show', [
	        'model' => $cms_menu	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_menus a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE name LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_menu = null;
		if($request->id > 0) { $cms_menu = Cms_menu::findOrFail($request->id); }
		else { 
			$cms_menu = new Cms_menu;
		}
	    

	    		
			    $cms_menu->id = $request->id?:0;
				
	    		
					    $cms_menu->name = $request->name;
		
	    		
					    $cms_menu->type = $request->type;
		
	    		
					    $cms_menu->path = $request->path;
		
	    		
					    $cms_menu->color = $request->color;
		
	    		
					    $cms_menu->icon = $request->icon;
		
	    		
					    $cms_menu->parent_id = $request->parent_id;
		
	    		
					    $cms_menu->is_active = $request->is_active;
		
	    		
					    $cms_menu->is_dashboard = $request->is_dashboard;
		
	    		
					    $cms_menu->id_cms_privileges = $request->id_cms_privileges;
		
	    		
					    $cms_menu->sorting = $request->sorting;
		
	    		
					    $cms_menu->created_at = $request->created_at;
		
	    		
					    $cms_menu->updated_at = $request->updated_at;
		
	    	    //$cms_menu->user_id = $request->user()->id;
	    $cms_menu->save();

	    return redirect('/cms_menus');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_menu = Cms_menu::findOrFail($id);

		$cms_menu->delete();
		return "OK";
	    
	}

	
}