<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Kategori_danasosial;

use DB;

class Kategori_danasosialsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('kategori_danasosials.index', []);
	}

	public function create(Request $request)
	{
	    return view('kategori_danasosials.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$kategori_danasosial = Kategori_danasosial::findOrFail($id);
	    return view('kategori_danasosials.add', [
	        'model' => $kategori_danasosial	    ]);
	}

	public function show(Request $request, $id)
	{
		$kategori_danasosial = Kategori_danasosial::findOrFail($id);
	    return view('kategori_danasosials.show', [
	        'model' => $kategori_danasosial	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM kategori_danasosial a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_kategori LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$kategori_danasosial = null;
		if($request->id > 0) { $kategori_danasosial = Kategori_danasosial::findOrFail($request->id); }
		else { 
			$kategori_danasosial = new Kategori_danasosial;
		}
	    

	    		
					    $kategori_danasosial->kategori_id = $request->kategori_id;
		
	    		
					    $kategori_danasosial->nama_kategori = $request->nama_kategori;
		
	    		
					    $kategori_danasosial->deskripsi_kategori = $request->deskripsi_kategori;
		
	    		
					    $kategori_danasosial->created_by = $request->created_by;
		
	    		
					    $kategori_danasosial->updated_at = $request->updated_at;
		
	    		
					    $kategori_danasosial->status = $request->status;
		
	    	    //$kategori_danasosial->user_id = $request->user()->id;
	    $kategori_danasosial->save();

	    return redirect('/kategori_danasosials');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$kategori_danasosial = Kategori_danasosial::findOrFail($id);

		$kategori_danasosial->delete();
		return "OK";
	    
	}

	
}