<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Versi_kitab_pujian;

use DB;

class Versi_kitab_pujiansController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('versi_kitab_pujians.index', []);
	}

	public function create(Request $request)
	{
	    return view('versi_kitab_pujians.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$versi_kitab_pujian = Versi_kitab_pujian::findOrFail($id);
	    return view('versi_kitab_pujians.add', [
	        'model' => $versi_kitab_pujian	    ]);
	}

	public function show(Request $request, $id)
	{
		$versi_kitab_pujian = Versi_kitab_pujian::findOrFail($id);
	    return view('versi_kitab_pujians.show', [
	        'model' => $versi_kitab_pujian	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM versi_kitab_pujian a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_versi LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$versi_kitab_pujian = null;
		if($request->id > 0) { $versi_kitab_pujian = Versi_kitab_pujian::findOrFail($request->id); }
		else { 
			$versi_kitab_pujian = new Versi_kitab_pujian;
		}
	    

	    		
					    $versi_kitab_pujian->versi_id = $request->versi_id;
		
	    		
					    $versi_kitab_pujian->nama_versi = $request->nama_versi;
		
	    		
					    $versi_kitab_pujian->created_by = $request->created_by;
		
	    		
					    $versi_kitab_pujian->status = $request->status;
		
	    		
					    $versi_kitab_pujian->kitabpujian_id = $request->kitabpujian_id;
		
	    	    //$versi_kitab_pujian->user_id = $request->user()->id;
	    $versi_kitab_pujian->save();

	    return redirect('/versi_kitab_pujians');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$versi_kitab_pujian = Versi_kitab_pujian::findOrFail($id);

		$versi_kitab_pujian->delete();
		return "OK";
	    
	}

	
}