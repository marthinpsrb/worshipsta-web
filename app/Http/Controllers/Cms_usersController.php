<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_user;

use DB;

class Cms_usersController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_users.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_users.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_user = Cms_user::findOrFail($id);
	    return view('cms_users.add', [
	        'model' => $cms_user	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_user = Cms_user::findOrFail($id);
	    return view('cms_users.show', [
	        'model' => $cms_user	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_users a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_user = null;
		if($request->id > 0) { $cms_user = Cms_user::findOrFail($request->id); }
		else { 
			$cms_user = new Cms_user;
		}
	    

	    		
			    $cms_user->id = $request->id?:0;
				
	    		
					    $cms_user->created_at = $request->created_at;
		
	    		
					    $cms_user->updated_at = $request->updated_at;
		
	    		
					    $cms_user->name = $request->name;
		
	    		
					    $cms_user->photo = $request->photo;
		
	    		
					    $cms_user->email = $request->email;
		
	    		
					    $cms_user->password = $request->password;
		
	    		
					    $cms_user->id_cms_privileges = $request->id_cms_privileges;
		
	    		
					    $cms_user->status = $request->status;
		
	    	    //$cms_user->user_id = $request->user()->id;
	    $cms_user->save();

	    return redirect('/cms_users');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_user = Cms_user::findOrFail($id);

		$cms_user->delete();
		return "OK";
	    
	}

	
}