<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_privileges_role;

use DB;

class Cms_privileges_rolesController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_privileges_roles.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_privileges_roles.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_privileges_role = Cms_privileges_role::findOrFail($id);
	    return view('cms_privileges_roles.add', [
	        'model' => $cms_privileges_role	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_privileges_role = Cms_privileges_role::findOrFail($id);
	    return view('cms_privileges_roles.show', [
	        'model' => $cms_privileges_role	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_privileges_roles a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_privileges_role = null;
		if($request->id > 0) { $cms_privileges_role = Cms_privileges_role::findOrFail($request->id); }
		else { 
			$cms_privileges_role = new Cms_privileges_role;
		}
	    

	    		
			    $cms_privileges_role->id = $request->id?:0;
				
	    		
					    $cms_privileges_role->created_at = $request->created_at;
		
	    		
					    $cms_privileges_role->updated_at = $request->updated_at;
		
	    		
					    $cms_privileges_role->is_visible = $request->is_visible;
		
	    		
					    $cms_privileges_role->is_create = $request->is_create;
		
	    		
					    $cms_privileges_role->is_read = $request->is_read;
		
	    		
					    $cms_privileges_role->is_edit = $request->is_edit;
		
	    		
					    $cms_privileges_role->is_delete = $request->is_delete;
		
	    		
					    $cms_privileges_role->id_cms_privileges = $request->id_cms_privileges;
		
	    		
					    $cms_privileges_role->id_cms_moduls = $request->id_cms_moduls;
		
	    	    //$cms_privileges_role->user_id = $request->user()->id;
	    $cms_privileges_role->save();

	    return redirect('/cms_privileges_roles');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_privileges_role = Cms_privileges_role::findOrFail($id);

		$cms_privileges_role->delete();
		return "OK";
	    
	}

	
}