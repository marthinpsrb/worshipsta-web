<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tata_ibadah;

use DB;

class Tata_ibadahsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('tata_ibadahs.index', []);
	}

	public function create(Request $request)
	{
	    return view('tata_ibadahs.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$tata_ibadah = Tata_ibadah::findOrFail($id);
	    return view('tata_ibadahs.add', [
	        'model' => $tata_ibadah	    ]);
	}

	public function show(Request $request, $id)
	{
		$tata_ibadah = Tata_ibadah::findOrFail($id);
	    return view('tata_ibadahs.show', [
	        'model' => $tata_ibadah	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM tata_ibadah a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_tata LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$tata_ibadah = null;
		if($request->id > 0) { $tata_ibadah = Tata_ibadah::findOrFail($request->id); }
		else { 
			$tata_ibadah = new Tata_ibadah;
		}
	    

	    		
					    $tata_ibadah->tataibadah _id = $request->tataibadah _id;
		
	    		
					    $tata_ibadah->nama_tata = $request->nama_tata;
		
	    		
					    $tata_ibadah->keterangan = $request->keterangan;
		
	    		
					    $tata_ibadah->created_by = $request->created_by;
		
	    		
					    $tata_ibadah->status = $request->status;
		
	    	    //$tata_ibadah->user_id = $request->user()->id;
	    $tata_ibadah->save();

	    return redirect('/tata_ibadahs');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$tata_ibadah = Tata_ibadah::findOrFail($id);

		$tata_ibadah->delete();
		return "OK";
	    
	}

	
}