<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Bookmark;

use DB;

class BookmarksController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('bookmarks.index', []);
	}

	public function create(Request $request)
	{
	    return view('bookmarks.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$bookmark = Bookmark::findOrFail($id);
	    return view('bookmarks.add', [
	        'model' => $bookmark	    ]);
	}

	public function show(Request $request, $id)
	{
		$bookmark = Bookmark::findOrFail($id);
	    return view('bookmarks.show', [
	        'model' => $bookmark	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM bookmark a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE isi_bookmark LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$bookmark = null;
		if($request->id > 0) { $bookmark = Bookmark::findOrFail($request->id); }
		else { 
			$bookmark = new Bookmark;
		}
	    

	    		
					    $bookmark->bookmark_id = $request->bookmark_id;
		
	    		
					    $bookmark->isi_bookmark = $request->isi_bookmark;
		
	    		
					    $bookmark->created_by = $request->created_by;
		
	    		
					    $bookmark->created_at = $request->created_at;
		
	    		
					    $bookmark->status = $request->status;
		
	    		
					    $bookmark->versi_id = $request->versi_id;
		
	    	    //$bookmark->user_id = $request->user()->id;
	    $bookmark->save();

	    return redirect('/bookmarks');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$bookmark = Bookmark::findOrFail($id);

		$bookmark->delete();
		return "OK";
	    
	}

	
}