<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_email_queue;

use DB;

class Cms_email_queuesController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_email_queues.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_email_queues.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_email_queue = Cms_email_queue::findOrFail($id);
	    return view('cms_email_queues.add', [
	        'model' => $cms_email_queue	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_email_queue = Cms_email_queue::findOrFail($id);
	    return view('cms_email_queues.show', [
	        'model' => $cms_email_queue	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_email_queues a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_email_queue = null;
		if($request->id > 0) { $cms_email_queue = Cms_email_queue::findOrFail($request->id); }
		else { 
			$cms_email_queue = new Cms_email_queue;
		}
	    

	    		
			    $cms_email_queue->id = $request->id?:0;
				
	    		
					    $cms_email_queue->created_at = $request->created_at;
		
	    		
					    $cms_email_queue->updated_at = $request->updated_at;
		
	    		
					    $cms_email_queue->send_at = $request->send_at;
		
	    		
					    $cms_email_queue->email_recipient = $request->email_recipient;
		
	    		
					    $cms_email_queue->email_from_email = $request->email_from_email;
		
	    		
					    $cms_email_queue->email_from_name = $request->email_from_name;
		
	    		
					    $cms_email_queue->email_cc_email = $request->email_cc_email;
		
	    		
					    $cms_email_queue->email_subject = $request->email_subject;
		
	    		
					    $cms_email_queue->email_content = $request->email_content;
		
	    		
					    $cms_email_queue->email_attachments = $request->email_attachments;
		
	    		
					    $cms_email_queue->is_sent = $request->is_sent;
		
	    	    //$cms_email_queue->user_id = $request->user()->id;
	    $cms_email_queue->save();

	    return redirect('/cms_email_queues');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_email_queue = Cms_email_queue::findOrFail($id);

		$cms_email_queue->delete();
		return "OK";
	    
	}

	
}