<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_privilege;

use DB;

class Cms_privilegesController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_privileges.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_privileges.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_privilege = Cms_privilege::findOrFail($id);
	    return view('cms_privileges.add', [
	        'model' => $cms_privilege	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_privilege = Cms_privilege::findOrFail($id);
	    return view('cms_privileges.show', [
	        'model' => $cms_privilege	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_privileges a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_privilege = null;
		if($request->id > 0) { $cms_privilege = Cms_privilege::findOrFail($request->id); }
		else { 
			$cms_privilege = new Cms_privilege;
		}
	    

	    		
			    $cms_privilege->id = $request->id?:0;
				
	    		
					    $cms_privilege->created_at = $request->created_at;
		
	    		
					    $cms_privilege->updated_at = $request->updated_at;
		
	    		
					    $cms_privilege->name = $request->name;
		
	    		
					    $cms_privilege->is_superadmin = $request->is_superadmin;
		
	    		
					    $cms_privilege->theme_color = $request->theme_color;
		
	    	    //$cms_privilege->user_id = $request->user()->id;
	    $cms_privilege->save();

	    return redirect('/cms_privileges');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_privilege = Cms_privilege::findOrFail($id);

		$cms_privilege->delete();
		return "OK";
	    
	}

	
}