<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Kitab_pujian_kategori;

use DB;

class Kitab_pujian_kategorisController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('kitab_pujian_kategoris.index', []);
	}

	public function create(Request $request)
	{
	    return view('kitab_pujian_kategoris.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$kitab_pujian_kategori = Kitab_pujian_kategori::findOrFail($id);
	    return view('kitab_pujian_kategoris.add', [
	        'model' => $kitab_pujian_kategori	    ]);
	}

	public function show(Request $request, $id)
	{
		$kitab_pujian_kategori = Kitab_pujian_kategori::findOrFail($id);
	    return view('kitab_pujian_kategoris.show', [
	        'model' => $kitab_pujian_kategori	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM kitab_pujian_kategori a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_kategori LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$kitab_pujian_kategori = null;
		if($request->id > 0) { $kitab_pujian_kategori = Kitab_pujian_kategori::findOrFail($request->id); }
		else { 
			$kitab_pujian_kategori = new Kitab_pujian_kategori;
		}
	    

	    		
					    $kitab_pujian_kategori->kitabpujian_id = $request->kitabpujian_id;
		
	    		
					    $kitab_pujian_kategori->nama_kategori = $request->nama_kategori;
		
	    		
					    $kitab_pujian_kategori->keterangan = $request->keterangan;
		
	    		
					    $kitab_pujian_kategori->created_by = $request->created_by;
		
	    		
					    $kitab_pujian_kategori->status = $request->status;
		
	    		
					    $kitab_pujian_kategori->versi_id = $request->versi_id;
		
	    	    //$kitab_pujian_kategori->user_id = $request->user()->id;
	    $kitab_pujian_kategori->save();

	    return redirect('/kitab_pujian_kategoris');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$kitab_pujian_kategori = Kitab_pujian_kategori::findOrFail($id);

		$kitab_pujian_kategori->delete();
		return "OK";
	    
	}

	
}