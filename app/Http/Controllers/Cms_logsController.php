<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_log;

use DB;

class Cms_logsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_logs.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_logs.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_log = Cms_log::findOrFail($id);
	    return view('cms_logs.add', [
	        'model' => $cms_log	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_log = Cms_log::findOrFail($id);
	    return view('cms_logs.show', [
	        'model' => $cms_log	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_logs a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_log = null;
		if($request->id > 0) { $cms_log = Cms_log::findOrFail($request->id); }
		else { 
			$cms_log = new Cms_log;
		}
	    

	    		
			    $cms_log->id = $request->id?:0;
				
	    		
					    $cms_log->created_at = $request->created_at;
		
	    		
					    $cms_log->updated_at = $request->updated_at;
		
	    		
					    $cms_log->ipaddress = $request->ipaddress;
		
	    		
					    $cms_log->useragent = $request->useragent;
		
	    		
					    $cms_log->url = $request->url;
		
	    		
					    $cms_log->description = $request->description;
		
	    		
					    $cms_log->id_cms_users = $request->id_cms_users;
		
	    	    //$cms_log->user_id = $request->user()->id;
	    $cms_log->save();

	    return redirect('/cms_logs');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_log = Cms_log::findOrFail($id);

		$cms_log->delete();
		return "OK";
	    
	}

	
}