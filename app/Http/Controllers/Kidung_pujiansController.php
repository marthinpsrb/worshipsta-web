<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Kidung_pujian;

use DB;

class Kidung_pujiansController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('kidung_pujians.index', []);
	}

	public function create(Request $request)
	{
	    return view('kidung_pujians.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$kidung_pujian = Kidung_pujian::findOrFail($id);
	    return view('kidung_pujians.add', [
	        'model' => $kidung_pujian	    ]);
	}

	public function show(Request $request, $id)
	{
		$kidung_pujian = Kidung_pujian::findOrFail($id);
	    return view('kidung_pujians.show', [
	        'model' => $kidung_pujian	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM kidung_pujian a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nomor LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$kidung_pujian = null;
		if($request->id > 0) { $kidung_pujian = Kidung_pujian::findOrFail($request->id); }
		else { 
			$kidung_pujian = new Kidung_pujian;
		}
	    

	    		
					    $kidung_pujian->kidungpujian_id = $request->kidungpujian_id;
		
	    		
					    $kidung_pujian->nomor = $request->nomor;
		
	    		
					    $kidung_pujian->nomor_notbalok = $request->nomor_notbalok;
		
	    		
					    $kidung_pujian->ayat = $request->ayat;
		
	    		
					    $kidung_pujian->lagu = $request->lagu;
		
	    		
					    $kidung_pujian->versi_id = $request->versi_id;
		
	    	    //$kidung_pujian->user_id = $request->user()->id;
	    $kidung_pujian->save();

	    return redirect('/kidung_pujians');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$kidung_pujian = Kidung_pujian::findOrFail($id);

		$kidung_pujian->delete();
		return "OK";
	    
	}

	
}