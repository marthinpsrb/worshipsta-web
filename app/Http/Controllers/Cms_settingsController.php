<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_setting;

use DB;

class Cms_settingsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_settings.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_settings.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_setting = Cms_setting::findOrFail($id);
	    return view('cms_settings.add', [
	        'model' => $cms_setting	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_setting = Cms_setting::findOrFail($id);
	    return view('cms_settings.show', [
	        'model' => $cms_setting	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_settings a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_setting = null;
		if($request->id > 0) { $cms_setting = Cms_setting::findOrFail($request->id); }
		else { 
			$cms_setting = new Cms_setting;
		}
	    

	    		
			    $cms_setting->id = $request->id?:0;
				
	    		
					    $cms_setting->created_at = $request->created_at;
		
	    		
					    $cms_setting->updated_at = $request->updated_at;
		
	    		
					    $cms_setting->name = $request->name;
		
	    		
					    $cms_setting->content = $request->content;
		
	    		
					    $cms_setting->content_input_type = $request->content_input_type;
		
	    		
					    $cms_setting->dataenum = $request->dataenum;
		
	    		
					    $cms_setting->helper = $request->helper;
		
	    		
					    $cms_setting->group_setting = $request->group_setting;
		
	    		
					    $cms_setting->label = $request->label;
		
	    	    //$cms_setting->user_id = $request->user()->id;
	    $cms_setting->save();

	    return redirect('/cms_settings');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_setting = Cms_setting::findOrFail($id);

		$cms_setting->delete();
		return "OK";
	    
	}

	
}