<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_apikey;

use DB;

class Cms_apikeysController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_apikeys.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_apikeys.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_apikey = Cms_apikey::findOrFail($id);
	    return view('cms_apikeys.add', [
	        'model' => $cms_apikey	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_apikey = Cms_apikey::findOrFail($id);
	    return view('cms_apikeys.show', [
	        'model' => $cms_apikey	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_apikey a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_apikey = null;
		if($request->id > 0) { $cms_apikey = Cms_apikey::findOrFail($request->id); }
		else { 
			$cms_apikey = new Cms_apikey;
		}
	    

	    		
			    $cms_apikey->id = $request->id?:0;
				
	    		
					    $cms_apikey->created_at = $request->created_at;
		
	    		
					    $cms_apikey->updated_at = $request->updated_at;
		
	    		
					    $cms_apikey->screetkey = $request->screetkey;
		
	    		
					    $cms_apikey->hit = $request->hit;
		
	    		
					    $cms_apikey->status = $request->status;
		
	    	    //$cms_apikey->user_id = $request->user()->id;
	    $cms_apikey->save();

	    return redirect('/cms_apikeys');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_apikey = Cms_apikey::findOrFail($id);

		$cms_apikey->delete();
		return "OK";
	    
	}

	
}