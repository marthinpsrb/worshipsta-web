<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_statistic;

use DB;

class Cms_statisticsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_statistics.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_statistics.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_statistic = Cms_statistic::findOrFail($id);
	    return view('cms_statistics.add', [
	        'model' => $cms_statistic	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_statistic = Cms_statistic::findOrFail($id);
	    return view('cms_statistics.show', [
	        'model' => $cms_statistic	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_statistics a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE name LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_statistic = null;
		if($request->id > 0) { $cms_statistic = Cms_statistic::findOrFail($request->id); }
		else { 
			$cms_statistic = new Cms_statistic;
		}
	    

	    		
			    $cms_statistic->id = $request->id?:0;
				
	    		
					    $cms_statistic->name = $request->name;
		
	    		
					    $cms_statistic->slug = $request->slug;
		
	    		
					    $cms_statistic->created_at = $request->created_at;
		
	    		
					    $cms_statistic->updated_at = $request->updated_at;
		
	    	    //$cms_statistic->user_id = $request->user()->id;
	    $cms_statistic->save();

	    return redirect('/cms_statistics');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_statistic = Cms_statistic::findOrFail($id);

		$cms_statistic->delete();
		return "OK";
	    
	}

	
}