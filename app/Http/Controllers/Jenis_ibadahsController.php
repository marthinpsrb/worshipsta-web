<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jenis_ibadah;

use DB;

class Jenis_ibadahsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('jenis_ibadahs.index', []);
	}

	public function create(Request $request)
	{
	    return view('jenis_ibadahs.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$jenis_ibadah = Jenis_ibadah::findOrFail($id);
	    return view('jenis_ibadahs.add', [
	        'model' => $jenis_ibadah	    ]);
	}

	public function show(Request $request, $id)
	{
		$jenis_ibadah = Jenis_ibadah::findOrFail($id);
	    return view('jenis_ibadahs.show', [
	        'model' => $jenis_ibadah	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM jenis_ibadah a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_ibadah LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$jenis_ibadah = null;
		if($request->id > 0) { $jenis_ibadah = Jenis_ibadah::findOrFail($request->id); }
		else { 
			$jenis_ibadah = new Jenis_ibadah;
		}
	    

	    		
					    $jenis_ibadah->jenisibadah_id = $request->jenisibadah_id;
		
	    		
					    $jenis_ibadah->nama_ibadah = $request->nama_ibadah;
		
	    		
					    $jenis_ibadah->keterangan = $request->keterangan;
		
	    		
					    $jenis_ibadah->created_by = $request->created_by;
		
	    		
					    $jenis_ibadah->status = $request->status;
		
	    	    //$jenis_ibadah->user_id = $request->user()->id;
	    $jenis_ibadah->save();

	    return redirect('/jenis_ibadahs');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$jenis_ibadah = Jenis_ibadah::findOrFail($id);

		$jenis_ibadah->delete();
		return "OK";
	    
	}

	
}