<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Kategori_gereja;

use DB;

class Kategori_gerejasController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('kategori_gerejas.index', []);
	}

	public function create(Request $request)
	{
	    return view('kategori_gerejas.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$kategori_gereja = Kategori_gereja::findOrFail($id);
	    return view('kategori_gerejas.add', [
	        'model' => $kategori_gereja	    ]);
	}

	public function show(Request $request, $id)
	{
		$kategori_gereja = Kategori_gereja::findOrFail($id);
	    return view('kategori_gerejas.show', [
	        'model' => $kategori_gereja	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM kategori_gereja a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE gereja LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$kategori_gereja = null;
		if($request->id > 0) { $kategori_gereja = Kategori_gereja::findOrFail($request->id); }
		else { 
			$kategori_gereja = new Kategori_gereja;
		}
	    

	    		
					    $kategori_gereja->kategorigereja_id = $request->kategorigereja_id;
		
	    		
					    $kategori_gereja->gereja = $request->gereja;
		
	    		
					    $kategori_gereja->keterangan = $request->keterangan;
		
	    		
					    $kategori_gereja->urutan_ibadah = $request->urutan_ibadah;
		
	    		
					    $kategori_gereja->urutan_ibadahpersekutuan = $request->urutan_ibadahpersekutuan;
		
	    		
					    $kategori_gereja->created_by = $request->created_by;
		
	    		
					    $kategori_gereja->status = $request->status;
		
	    	    //$kategori_gereja->user_id = $request->user()->id;
	    $kategori_gereja->save();

	    return redirect('/kategori_gerejas');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$kategori_gereja = Kategori_gereja::findOrFail($id);

		$kategori_gereja->delete();
		return "OK";
	    
	}

	
}