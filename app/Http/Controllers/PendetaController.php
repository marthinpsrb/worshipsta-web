<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Pendetum;

use DB;

class PendetaController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('pendeta.index', []);
	}

	public function create(Request $request)
	{
	    return view('pendeta.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$pendetum = Pendetum::findOrFail($id);
	    return view('pendeta.add', [
	        'model' => $pendetum	    ]);
	}

	public function show(Request $request, $id)
	{
		$pendetum = Pendetum::findOrFail($id);
	    return view('pendeta.show', [
	        'model' => $pendetum	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM pendeta a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_pendeta LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$pendetum = null;
		if($request->id > 0) { $pendetum = Pendetum::findOrFail($request->id); }
		else { 
			$pendetum = new Pendetum;
		}
	    

	    		
					    $pendetum->pendeta_id = $request->pendeta_id;
		
	    		
					    $pendetum->nama_pendeta = $request->nama_pendeta;
		
	    		
					    $pendetum->email = $request->email;
		
	    		
					    $pendetum->foto_gereja = $request->foto_gereja;
		
	    		
					    $pendetum->no_hp = $request->no_hp;
		
	    		
					    $pendetum->tahun_masuk = $request->tahun_masuk;
		
	    		
					    $pendetum->status = $request->status;
		
	    		
					    $pendetum->created_by = $request->created_by;
		
	    		
					    $pendetum->user_id = $request->user_id;
		
	    	    //$pendetum->user_id = $request->user()->id;
	    $pendetum->save();

	    return redirect('/pendeta');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$pendetum = Pendetum::findOrFail($id);

		$pendetum->delete();
		return "OK";
	    
	}

	
}