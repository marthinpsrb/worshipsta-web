<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Profile;

use DB;

class ProfilesController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('profiles.index', []);
	}

	public function create(Request $request)
	{
	    return view('profiles.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$profile = Profile::findOrFail($id);
	    return view('profiles.add', [
	        'model' => $profile	    ]);
	}

	public function show(Request $request, $id)
	{
		$profile = Profile::findOrFail($id);
	    return view('profiles.show', [
	        'model' => $profile	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM profile a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_lengkap LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$profile = null;
		if($request->id > 0) { $profile = Profile::findOrFail($request->id); }
		else { 
			$profile = new Profile;
		}
	    

	    		
					    $profile->profile_id = $request->profile_id;
		
	    		
					    $profile->nama_lengkap = $request->nama_lengkap;
		
	    		
					    $profile->nomor_identitas = $request->nomor_identitas;
		
	    		
					    $profile->jemaat_gereja = $request->jemaat_gereja;
		
	    		
					    $profile->foto_gereja = $request->foto_gereja;
		
	    		
					    $profile->created_by = $request->created_by;
		
	    		
					    $profile->updated_at = $request->updated_at;
		
	    	    //$profile->user_id = $request->user()->id;
	    $profile->save();

	    return redirect('/profiles');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$profile = Profile::findOrFail($id);

		$profile->delete();
		return "OK";
	    
	}

	
}