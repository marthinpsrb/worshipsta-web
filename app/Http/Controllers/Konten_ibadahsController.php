<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Konten_ibadah;

use DB;

class Konten_ibadahsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('konten_ibadahs.index', []);
	}

	public function create(Request $request)
	{
	    return view('konten_ibadahs.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$konten_ibadah = Konten_ibadah::findOrFail($id);
	    return view('konten_ibadahs.add', [
	        'model' => $konten_ibadah	    ]);
	}

	public function show(Request $request, $id)
	{
		$konten_ibadah = Konten_ibadah::findOrFail($id);
	    return view('konten_ibadahs.show', [
	        'model' => $konten_ibadah	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM konten_ibadah a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE konten LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$konten_ibadah = null;
		if($request->id > 0) { $konten_ibadah = Konten_ibadah::findOrFail($request->id); }
		else { 
			$konten_ibadah = new Konten_ibadah;
		}
	    

	    		
					    $konten_ibadah->konten_id = $request->konten_id;
		
	    		
					    $konten_ibadah->konten = $request->konten;
		
	    		
					    $konten_ibadah->waktu = $request->waktu;
		
	    		
					    $konten_ibadah->link_download = $request->link_download;
		
	    		
					    $konten_ibadah->created_by = $request->created_by;
		
	    		
					    $konten_ibadah->status = $request->status;
		
	    		
					    $konten_ibadah->tataibadah_id = $request->tataibadah_id;
		
	    		
					    $konten_ibadah->jadwal_id = $request->jadwal_id;
		
	    		
					    $konten_ibadah->jenisibadah_id = $request->jenisibadah_id;
		
	    	    //$konten_ibadah->user_id = $request->user()->id;
	    $konten_ibadah->save();

	    return redirect('/konten_ibadahs');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$konten_ibadah = Konten_ibadah::findOrFail($id);

		$konten_ibadah->delete();
		return "OK";
	    
	}

	
}