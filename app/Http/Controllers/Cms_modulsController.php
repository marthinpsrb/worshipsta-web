<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_modul;

use DB;

class Cms_modulsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_moduls.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_moduls.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_modul = Cms_modul::findOrFail($id);
	    return view('cms_moduls.add', [
	        'model' => $cms_modul	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_modul = Cms_modul::findOrFail($id);
	    return view('cms_moduls.show', [
	        'model' => $cms_modul	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_moduls a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_modul = null;
		if($request->id > 0) { $cms_modul = Cms_modul::findOrFail($request->id); }
		else { 
			$cms_modul = new Cms_modul;
		}
	    

	    		
			    $cms_modul->id = $request->id?:0;
				
	    		
					    $cms_modul->created_at = $request->created_at;
		
	    		
					    $cms_modul->updated_at = $request->updated_at;
		
	    		
					    $cms_modul->name = $request->name;
		
	    		
					    $cms_modul->icon = $request->icon;
		
	    		
					    $cms_modul->path = $request->path;
		
	    		
					    $cms_modul->table_name = $request->table_name;
		
	    		
					    $cms_modul->controller = $request->controller;
		
	    		
					    $cms_modul->is_protected = $request->is_protected;
		
	    		
					    $cms_modul->is_active = $request->is_active;
		
	    	    //$cms_modul->user_id = $request->user()->id;
	    $cms_modul->save();

	    return redirect('/cms_moduls');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_modul = Cms_modul::findOrFail($id);

		$cms_modul->delete();
		return "OK";
	    
	}

	
}