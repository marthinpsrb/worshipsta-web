<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Dana_sosial_detail;

use DB;

class Dana_sosial_detailsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('dana_sosial_details.index', []);
	}

	public function create(Request $request)
	{
	    return view('dana_sosial_details.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$dana_sosial_detail = Dana_sosial_detail::findOrFail($id);
	    return view('dana_sosial_details.add', [
	        'model' => $dana_sosial_detail	    ]);
	}

	public function show(Request $request, $id)
	{
		$dana_sosial_detail = Dana_sosial_detail::findOrFail($id);
	    return view('dana_sosial_details.show', [
	        'model' => $dana_sosial_detail	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM dana_sosial_detail a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE gambar_utama LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$dana_sosial_detail = null;
		if($request->id > 0) { $dana_sosial_detail = Dana_sosial_detail::findOrFail($request->id); }
		else { 
			$dana_sosial_detail = new Dana_sosial_detail;
		}
	    

	    		
					    $dana_sosial_detail->detail_id = $request->detail_id;
		
	    		
					    $dana_sosial_detail->gambar_utama = $request->gambar_utama;
		
	    		
					    $dana_sosial_detail->nomor_kontak = $request->nomor_kontak;
		
	    		
					    $dana_sosial_detail->link_video = $request->link_video;
		
	    		
					    $dana_sosial_detail->deskiripsi_singkat = $request->deskiripsi_singkat;
		
	    		
					    $dana_sosial_detail->deskirpsi_lengkap = $request->deskirpsi_lengkap;
		
	    		
					    $dana_sosial_detail->danasosial_id = $request->danasosial_id;
		
	    	    //$dana_sosial_detail->user_id = $request->user()->id;
	    $dana_sosial_detail->save();

	    return redirect('/dana_sosial_details');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$dana_sosial_detail = Dana_sosial_detail::findOrFail($id);

		$dana_sosial_detail->delete();
		return "OK";
	    
	}

	
}