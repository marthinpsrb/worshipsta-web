<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_email_template;

use DB;

class Cms_email_templatesController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_email_templates.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_email_templates.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_email_template = Cms_email_template::findOrFail($id);
	    return view('cms_email_templates.add', [
	        'model' => $cms_email_template	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_email_template = Cms_email_template::findOrFail($id);
	    return view('cms_email_templates.show', [
	        'model' => $cms_email_template	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_email_templates a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE name LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_email_template = null;
		if($request->id > 0) { $cms_email_template = Cms_email_template::findOrFail($request->id); }
		else { 
			$cms_email_template = new Cms_email_template;
		}
	    

	    		
			    $cms_email_template->id = $request->id?:0;
				
	    		
					    $cms_email_template->name = $request->name;
		
	    		
					    $cms_email_template->slug = $request->slug;
		
	    		
					    $cms_email_template->subject = $request->subject;
		
	    		
					    $cms_email_template->content = $request->content;
		
	    		
					    $cms_email_template->description = $request->description;
		
	    		
					    $cms_email_template->from_name = $request->from_name;
		
	    		
					    $cms_email_template->from_email = $request->from_email;
		
	    		
					    $cms_email_template->cc_email = $request->cc_email;
		
	    		
					    $cms_email_template->created_at = $request->created_at;
		
	    		
					    $cms_email_template->updated_at = $request->updated_at;
		
	    	    //$cms_email_template->user_id = $request->user()->id;
	    $cms_email_template->save();

	    return redirect('/cms_email_templates');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_email_template = Cms_email_template::findOrFail($id);

		$cms_email_template->delete();
		return "OK";
	    
	}

	
}