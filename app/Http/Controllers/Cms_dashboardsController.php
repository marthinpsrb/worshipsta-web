<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cms_dashboard;

use DB;

class Cms_dashboardsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('cms_dashboards.index', []);
	}

	public function create(Request $request)
	{
	    return view('cms_dashboards.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$cms_dashboard = Cms_dashboard::findOrFail($id);
	    return view('cms_dashboards.add', [
	        'model' => $cms_dashboard	    ]);
	}

	public function show(Request $request, $id)
	{
		$cms_dashboard = Cms_dashboard::findOrFail($id);
	    return view('cms_dashboards.show', [
	        'model' => $cms_dashboard	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM cms_dashboard a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE created_at LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$cms_dashboard = null;
		if($request->id > 0) { $cms_dashboard = Cms_dashboard::findOrFail($request->id); }
		else { 
			$cms_dashboard = new Cms_dashboard;
		}
	    

	    		
			    $cms_dashboard->id = $request->id?:0;
				
	    		
					    $cms_dashboard->created_at = $request->created_at;
		
	    		
					    $cms_dashboard->updated_at = $request->updated_at;
		
	    		
					    $cms_dashboard->name = $request->name;
		
	    		
					    $cms_dashboard->id_cms_privileges = $request->id_cms_privileges;
		
	    		
					    $cms_dashboard->content = $request->content;
		
	    	    //$cms_dashboard->user_id = $request->user()->id;
	    $cms_dashboard->save();

	    return redirect('/cms_dashboards');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$cms_dashboard = Cms_dashboard::findOrFail($id);

		$cms_dashboard->delete();
		return "OK";
	    
	}

	
}