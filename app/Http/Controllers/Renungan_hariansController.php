<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Renungan_harian;

use DB;

class Renungan_hariansController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('renungan_harians.index', []);
	}

	public function create(Request $request)
	{
	    return view('renungan_harians.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$renungan_harian = Renungan_harian::findOrFail($id);
	    return view('renungan_harians.add', [
	        'model' => $renungan_harian	    ]);
	}

	public function show(Request $request, $id)
	{
		$renungan_harian = Renungan_harian::findOrFail($id);
	    return view('renungan_harians.show', [
	        'model' => $renungan_harian	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM renungan_harian a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE judul LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$renungan_harian = null;
		if($request->id > 0) { $renungan_harian = Renungan_harian::findOrFail($request->id); }
		else { 
			$renungan_harian = new Renungan_harian;
		}
	    

	    		
					    $renungan_harian->renungan_id = $request->renungan_id;
		
	    		
					    $renungan_harian->judul = $request->judul;
		
	    		
					    $renungan_harian->ayat_renungan = $request->ayat_renungan;
		
	    		
					    $renungan_harian->isi_renungan = $request->isi_renungan;
		
	    		
					    $renungan_harian->created_at = $request->created_at;
		
	    		
					    $renungan_harian->status = $request->status;
		
	    		
					    $renungan_harian->rating = $request->rating;
		
	    		
					    $renungan_harian->user_id = $request->user_id;
		
	    	    //$renungan_harian->user_id = $request->user()->id;
	    $renungan_harian->save();

	    return redirect('/renungan_harians');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$renungan_harian = Renungan_harian::findOrFail($id);

		$renungan_harian->delete();
		return "OK";
	    
	}

	
}