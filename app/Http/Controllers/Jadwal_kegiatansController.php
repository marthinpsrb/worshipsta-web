<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jadwal_kegiatan;

use DB;

class Jadwal_kegiatansController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
	{
	    return view('jadwal_kegiatans.index', []);
	}

	public function create(Request $request)
	{
	    return view('jadwal_kegiatans.add', [
	        []
	    ]);
	}

	public function edit(Request $request, $id)
	{
		$jadwal_kegiatan = Jadwal_kegiatan::findOrFail($id);
	    return view('jadwal_kegiatans.add', [
	        'model' => $jadwal_kegiatan	    ]);
	}

	public function show(Request $request, $id)
	{
		$jadwal_kegiatan = Jadwal_kegiatan::findOrFail($id);
	    return view('jadwal_kegiatans.show', [
	        'model' => $jadwal_kegiatan	    ]);
	}

	public function grid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM jadwal_kegiatan a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE nama_jadwal LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function update(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$jadwal_kegiatan = null;
		if($request->id > 0) { $jadwal_kegiatan = Jadwal_kegiatan::findOrFail($request->id); }
		else { 
			$jadwal_kegiatan = new Jadwal_kegiatan;
		}
	    

	    		
					    $jadwal_kegiatan->jadwal_id = $request->jadwal_id;
		
	    		
					    $jadwal_kegiatan->nama_jadwal = $request->nama_jadwal;
		
	    		
					    $jadwal_kegiatan->keterangan = $request->keterangan;
		
	    		
					    $jadwal_kegiatan->waktu = $request->waktu;
		
	    		
					    $jadwal_kegiatan->created_by = $request->created_by;
		
	    		
					    $jadwal_kegiatan->status = $request->status;
		
	    		
					    $jadwal_kegiatan->konten_id = $request->konten_id;
		
	    		
					    $jadwal_kegiatan->gereja_id = $request->gereja_id;
		
	    	    //$jadwal_kegiatan->user_id = $request->user()->id;
	    $jadwal_kegiatan->save();

	    return redirect('/jadwal_kegiatans');

	}

	public function store(Request $request)
	{
		return $this->update($request);
	}

	public function destroy(Request $request, $id) {
		
		$jadwal_kegiatan = Jadwal_kegiatan::findOrFail($id);

		$jadwal_kegiatan->delete();
		return "OK";
	    
	}

	
}