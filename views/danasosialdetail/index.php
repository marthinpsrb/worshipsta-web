<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DanaSosialDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dana Sosial Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dana-sosial-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dana Sosial Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'gambar_utama',
            'nomor_kontak',
            'link_video',
            'deskiripsi_singkat:ntext',
            // 'deskirpsi_lengkap:ntext',
            // 'danasosial_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
