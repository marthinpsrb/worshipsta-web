<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DanaSosialDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dana-sosial-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'gambar_utama') ?>

    <?= $form->field($model, 'nomor_kontak') ?>

    <?= $form->field($model, 'link_video') ?>

    <?= $form->field($model, 'deskiripsi_singkat') ?>

    <?php // echo $form->field($model, 'deskirpsi_lengkap') ?>

    <?php // echo $form->field($model, 'danasosial_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
