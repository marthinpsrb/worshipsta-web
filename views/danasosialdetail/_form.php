<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DanaSosialDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dana-sosial-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gambar_utama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nomor_kontak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_video')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskiripsi_singkat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'deskirpsi_lengkap')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'danasosial_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
