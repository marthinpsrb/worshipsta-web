<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JenisIbadahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis Ibadahs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-ibadah-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Jenis Ibadah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama_ibadah',
            'keterangan:ntext',
            'status',
            'gereja_id',
            // 'waktu',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
