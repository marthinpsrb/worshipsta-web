<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KategoriDanaSosial */

$this->title = 'Create Kategori Dana Sosial';
$this->params['breadcrumbs'][] = ['label' => 'Kategori Dana Sosials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-dana-sosial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
