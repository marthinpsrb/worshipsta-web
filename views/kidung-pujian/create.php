<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KidungPujian */

$this->title = 'Create Kidung Pujian';
$this->params['breadcrumbs'][] = ['label' => 'Kidung Pujians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="features">
        <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
