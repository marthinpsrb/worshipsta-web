<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KidungPujian */
/* @var $form yii\widgets\ActiveForm */
?>

<section id="features">
        <div class="container">
            
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nomor')->textInput() ?>

    <?= $form->field($model, 'nomor_notbalok')->textInput() ?>

    <?= $form->field($model, 'ayat')->textInput() ?>

    <?= $form->field($model, 'lagu')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'versi_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
