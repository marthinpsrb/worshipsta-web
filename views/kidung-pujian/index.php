<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KidungPujianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kidung Pujians';
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="features">
        <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kidung Pujian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomor',
            'nomor_notbalok',
            'ayat',
            'lagu:ntext',
            // 'versi_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</section>
