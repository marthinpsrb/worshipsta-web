<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KitabSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kitabs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitab-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kitab', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kitab',
            'kategori_kitab',
            'versi_kitab',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
