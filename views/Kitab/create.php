<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kitab */

$this->title = 'Create Kitab';
$this->params['breadcrumbs'][] = ['label' => 'Kitabs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitab-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
