<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kidung */

$this->title = 'Create Kidung';
$this->params['breadcrumbs'][] = ['label' => 'Kidungs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kidung-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
