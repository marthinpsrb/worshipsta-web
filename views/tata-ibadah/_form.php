<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TataIbadah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tata-ibadah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_jenistataibadah')->textInput() ?>

    <?= $form->field($model, 'id_gereja')->textInput() ?>

    <?= $form->field($model, 'urutan_tataibadah')->textInput() ?>

    <?= $form->field($model, 'judul_tataibadah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'konten_tataibadah')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
