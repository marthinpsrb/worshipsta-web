<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TataIbadah */

$this->title = 'Update Tata Ibadah: ' . $model->id_tataibadah;
$this->params['breadcrumbs'][] = ['label' => 'Tata Ibadahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tataibadah, 'url' => ['view', 'id' => $model->id_tataibadah]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tata-ibadah-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
