<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TataIbadahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tata Ibadahs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tata-ibadah-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tata Ibadah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_tataibadah',
            'id_jenistataibadah',
            'id_gereja',
            'urutan_tataibadah',
            'judul_tataibadah',
            // 'konten_tataibadah:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
