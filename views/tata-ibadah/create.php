<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TataIbadah */

$this->title = 'Create Tata Ibadah';
$this->params['breadcrumbs'][] = ['label' => 'Tata Ibadahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tata-ibadah-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
