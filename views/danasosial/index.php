<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DanaSosialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dana Sosials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dana-sosial-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dana Sosial', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'judul',
            'created_by',
            'target_dana',
            'deadline',
            // 'alamat:ntext',
            // 'status_danasosial',
            // 'status',
            // 'kategori_id',
            // 'user_id',
            // 'detail_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
