<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DanaSosial */

$this->title = 'Update Dana Sosial: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dana Sosials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dana-sosial-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
