<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KitabIsi */

$this->title = 'Create Kitab Isi';
$this->params['breadcrumbs'][] = ['label' => 'Kitab Isis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitab-isi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
