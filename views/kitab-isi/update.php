<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KitabIsi */

$this->title = 'Update Kitab Isi: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kitab Isis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kitab-isi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
