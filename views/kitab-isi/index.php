<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KitabIsiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kitab Isis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitab-isi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kitab Isi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kitab',
            'pasal',
            'ayat',
            'firman',
            // 'versi_kitab',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
