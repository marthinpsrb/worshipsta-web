<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KitabIsi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kitab-isi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kitab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pasal')->textInput() ?>

    <?= $form->field($model, 'ayat')->textInput() ?>

    <?= $form->field($model, 'firman')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'versi_kitab')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
