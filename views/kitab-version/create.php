<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KitabVersion */

$this->title = 'Create Kitab Version';
$this->params['breadcrumbs'][] = ['label' => 'Kitab Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitab-version-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
