<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KitabVersion */

$this->title = 'Update Kitab Version: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kitab Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kitab-version-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
