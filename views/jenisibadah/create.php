<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JenisIbadah */

$this->title = 'Create Jenis Ibadah';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Ibadahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-ibadah-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
