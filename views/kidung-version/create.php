<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KidungVersion */

$this->title = 'Create Kidung Version';
$this->params['breadcrumbs'][] = ['label' => 'Kidung Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kidung-version-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
