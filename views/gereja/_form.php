<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\controllers\GerejaController;


/* @var $this yii\web\View */
/* @var $model app\models\Gereja */
/* @var $form yii\widgets\ActiveForm */
?>

<section id="features">
        <div class="container">
  <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'nama_gereja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_gereja')->fileInput(); ?>

    <?= $form->field($model, 'sejarah_gereja')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

<div id="map"></div>

<script>
      function initMap() {
        var uluru = {lat: 2.609325, lng: 98.803663};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          draggable: true
        });

        var position = new google.maps.event.addListener(marker, 'dragend', function(){
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            $('#gereja-latitude').val(lat);
            $('#gereja-longitude').val(lng);

        });
    }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDviXwMB83TIYnzUtnttigVZ859vtnX7w&callback=initMap">
    </script>

       <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js">
</script>

    <?= $form->field($model, 'latitude')->textInput(['readonly' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['readonly' => true]) ?>

    <?= $form->field($model, 'kategorigereja_id')->dropDownList($kategorigereja) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</section>