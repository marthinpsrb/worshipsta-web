<?php

use yii\helpers\Html;
use app\controllers\GerejaController;	


/* @var $this yii\web\View */
/* @var $model app\models\Gereja */

$this->title = 'Create Gereja';
$this->params['breadcrumbs'][] = ['label' => 'Gerejas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="features">
        <div class="container"> 

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'kategorigereja' => $kategorigereja,
    ]) ?>

</div>
</section>
