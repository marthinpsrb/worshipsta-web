<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\controllers\GerejaController;

/* @var $this yii\web\View */
/* @var $model app\models\Gereja */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gerejas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="features">
        <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama_gereja',
            'foto_gereja',
            'sejarah_gereja:ntext',
            'alamat:ntext',
            'latitude',
            'longitude',
            'status',
            'created_by',
            'updated_at',
            'kategorigereja_id',
            'user_id',
        ],
    ]) ?>

</div>
</section>
