<?php

use yii\helpers\Html;
use app\controllers\GerejaController;


/* @var $this yii\web\View */
/* @var $model app\models\Gereja */

$this->title = 'Update Gereja: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gerejas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<section id="features">
        <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'kategorigereja' => $kategorigereja,
    ]) ?>

</div>
</section>