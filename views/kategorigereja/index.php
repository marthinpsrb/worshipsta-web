<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriGerejaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategori Gerejas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-gereja-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kategori Gereja', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'gereja',
            'keterangan:ntext',
            'urutan_ibadah',
            'urutan_ibadahpersekutuan',
            // 'created_by',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
