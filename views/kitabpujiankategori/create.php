<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KitabPujianKategori */

$this->title = 'Create Kitab Pujian Kategori';
$this->params['breadcrumbs'][] = ['label' => 'Kitab Pujian Kategoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitab-pujian-kategori-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
