<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KitabPujianKategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kitab Pujian Kategoris';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kitab-pujian-kategori-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kitab Pujian Kategori', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama_kategori',
            'keterangan:ntext',
            'created_by',
            'status',
            // 'versi_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
