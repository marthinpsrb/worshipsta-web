<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JadwalKegiatanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-kegiatan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama_jadwal') ?>

    <?= $form->field($model, 'keterangan') ?>

    <?= $form->field($model, 'waktu') ?>

    <?= $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'gereja_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
