<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pendeta */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pendetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pendeta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama_pendeta',
            'email:email',
            'foto_gereja',
            'no_hp',
            'tahun_masuk',
            'status',
            'created_by',
            'user_id',
        ],
    ]) ?>

</div>
