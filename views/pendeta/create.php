<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pendeta */

$this->title = 'Create Pendeta';
$this->params['breadcrumbs'][] = ['label' => 'Pendetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pendeta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
