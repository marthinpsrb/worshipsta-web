<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PendetaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pendetas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pendeta-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pendeta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama_pendeta',
            'email:email',
            'foto_gereja',
            'no_hp',
            // 'tahun_masuk',
            // 'status',
            // 'created_by',
            // 'user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
