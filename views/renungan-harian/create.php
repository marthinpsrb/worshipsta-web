<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RenunganHarian */

$this->title = 'Create Renungan Harian';
$this->params['breadcrumbs'][] = ['label' => 'Renungan Harians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="renungan-harian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
