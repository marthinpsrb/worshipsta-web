<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RenunganHarianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Renungan Harians';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="renungan-harian-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Renungan Harian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'judul',
            'ayat_renungan',
            'isi_renungan:ntext',
            'created_at',
            // 'status',
            // 'user_id',
            // 'sumber',
            // 'gambar',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
