<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KidungIsi */

$this->title = 'Create Kidung Isi';
$this->params['breadcrumbs'][] = ['label' => 'Kidung Isis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kidung-isi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
