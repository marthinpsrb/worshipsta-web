<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KidungIsiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kidung Isis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kidung-isi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kidung Isi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomor_kidungpujian',
            'ayat',
            'isi',
            'versi_kidung',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
