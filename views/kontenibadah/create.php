<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KontenIbadah */

$this->title = 'Create Konten Ibadah';
$this->params['breadcrumbs'][] = ['label' => 'Konten Ibadahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konten-ibadah-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
