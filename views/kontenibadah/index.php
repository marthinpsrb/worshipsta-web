<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KontenIbadahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konten Ibadahs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konten-ibadah-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Konten Ibadah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'konten:ntext',
            'waktu',
            'link_download',
            'created_by',
            // 'status',
            // 'tataibadah_id',
            // 'jadwal_id',
            // 'jenisibadah_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
