<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KontenIbadahSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konten-ibadah-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'konten') ?>

    <?= $form->field($model, 'waktu') ?>

    <?= $form->field($model, 'link_download') ?>

    <?= $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'tataibadah_id') ?>

    <?php // echo $form->field($model, 'jadwal_id') ?>

    <?php // echo $form->field($model, 'jenisibadah_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
