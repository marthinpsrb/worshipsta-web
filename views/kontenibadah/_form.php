<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KontenIbadah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konten-ibadah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'konten')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'waktu')->textInput() ?>

    <?= $form->field($model, 'link_download')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'tataibadah_id')->textInput() ?>

    <?= $form->field($model, 'jadwal_id')->textInput() ?>

    <?= $form->field($model, 'jenisibadah_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
