<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$asset = AppAsset::register($this);
$baseUrl = $asset->baseUrl;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    
    <div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'WISTA',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Pendeta', 'url' => ['/pendeta/index/']],
            ['label' => 'Gereja', 'url' =>  ['/gereja/index']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>x

<?= $content ?>

    <!-- Footer start -->
    <footer id="footer">
        <div class="container">
            <!-- footer Content Inner -->
            <div class="row margin-sm">
                <div class="col-lg-12 text-center">
                    <ul class="list-unstyled footer-items footer-social no-margin">
                        <li class="footer-social-item">
                            <a href="#" class="cbutton cbutton--effect-ivana">
                                <i class="cbutton__icon fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="footer-social-item">
                            <a href="#" class="cbutton cbutton--effect-ivana">
                                <i class="cbutton__icon fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="footer-social-item">
                            <a href="#" class="cbutton cbutton--effect-ivana">
                                <i class="cbutton__icon fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li class="footer-social-item">
                            <a href="#" class="cbutton cbutton--effect-ivana">
                                <i class="cbutton__icon fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                    <span>Copyright © 2015 <a href="http://themes.audaindesigns.com" >Audain Designs</a></span>
                </div>
            </div>
            <!-- /footer Content Inner -->
        </div>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
