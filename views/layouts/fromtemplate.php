    <nav id="infinityNav" class="navmenu menu-side navmenu-default navmenu-fixed-left offcanvas" role="navigation">
        <ul class="list-unstyled push-menu-items pm-title text-center">
            <li class="push-menu-title">
                <span>WISTA</span>
            </li>
        </ul>
        <ul class="list-unstyled push-menu-items push-menu-social text-center">
            <li class="push-menu-social-item">
                <a href="#" class="cbutton cbutton--effect-ivana">
                    <i class="cbutton__icon fa fa-twitter"></i>
                </a>
            </li>
            <li class="push-menu-social-item">
                <a href="#" class="cbutton cbutton--effect-ivana">
                    <i class="cbutton__icon fa fa-facebook"></i>
                </a>
            </li>
            <li class="push-menu-social-item">
                <a href="#" class="cbutton cbutton--effect-ivana">
                    <i class="cbutton__icon fa fa-google-plus"></i>
                </a>
            </li>
            <li class="push-menu-social-item">
                <a href="#" class="cbutton cbutton--effect-ivana">
                    <i class="cbutton__icon fa fa-linkedin"></i>
                </a>
            </li>
        </ul>
        <div id="push-menu-nav">
            <ul class="list-unstyled push-menu-items text-center">
                <li class="push-menu-item">
                    <a href="/wista/web/index.php" class="smooth" >home</a>
                </li>
                <li class="push-menu-item">
                    <a href="#features" class="smooth">features</a>
                </li>
                <li class="push-menu-item">
                    <a href="#whyus" class="smooth">why us?</a>
                </li>
                <li class="push-menu-item">
                    <a href="#information" class="smooth">information</a>
                </li>
                <li class="push-menu-item">
                    <a href="#gallery" class="smooth">gallery</a>
                </li>
                <li class="push-menu-item">
                    <a href="#testimonials" class="smooth">testimonials</a>
                </li>
                <li class="push-menu-item">
                    <a href="#video" class="smooth">video</a>
                </li>
                <li class="push-menu-item">
                    <a href="#contact" class="smooth">contact</a>
                </li>
            </ul>
        </div>
        <div class="menu-store-btn-container">
            <ul class="available-on-menu list-unstyled">
                <li>
                    <span class="landing-availability">Available for</span>
                </li>
                <li>
                    <i class="fa fa-tablet"></i>
                </li>
                <li>
                    <i class="fa fa-mobile-phone"></i>
                </li>
            </ul>
            <a href="#" class="btn btn-store-menu"><img src="<?= $baseUrl; ?>/img/stores/btn-app-store.png" class="img-responsive" alt="app store"></a>
            <a href="#" class="btn btn-store-menu"><img src="<?= $baseUrl; ?>/img/stores/btn-google-play.png" class="img-responsive" alt="google store"></a>
            <a href="#" class="btn btn-store-menu"><img src="<?= $baseUrl; ?>/img/stores/btn-windows-store.png" class="img-responsive" alt="windows store"></a>
            
        </div>
    </nav>
    <div class="push-menu-container">
        <div class="navbar navbar-default navbar-fixed-top push-wrapper">
            <div class="push-menu-button" data-toggle="offcanvas" data-target="#infinityNav" data-canvas="body">
                <button type="button" class="navbar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <span class="menu-text">Menu</span>
            </div>
        </div>
    </div>