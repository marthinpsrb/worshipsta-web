<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VersiKitabPujianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Versi Kitab Pujians';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="versi-kitab-pujian-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Versi Kitab Pujian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama_versi',
            'created_by',
            'status',
            'kitabpujian_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
