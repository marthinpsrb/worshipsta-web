<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VersiKitabPujian */

$this->title = 'Create Versi Kitab Pujian';
$this->params['breadcrumbs'][] = ['label' => 'Versi Kitab Pujians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="versi-kitab-pujian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
