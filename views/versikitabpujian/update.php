<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VersiKitabPujian */

$this->title = 'Update Versi Kitab Pujian: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Versi Kitab Pujians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="versi-kitab-pujian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
