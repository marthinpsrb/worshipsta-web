<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JenisTataibadah */

$this->title = 'Update Jenis Tataibadah: ' . $model->id_jenistataibadah;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Tataibadahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_jenistataibadah, 'url' => ['view', 'id' => $model->id_jenistataibadah]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-tataibadah-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
