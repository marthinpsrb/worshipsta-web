<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JenisTataibadah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-tataibadah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_jenisibadah')->textInput() ?>

    <?= $form->field($model, 'tema_ibadah')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tanggal')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
