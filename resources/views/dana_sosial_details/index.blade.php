@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('dana_sosial_details') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('dana_sosial_details') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Detail Id</th>
                                        <th>Gambar Utama</th>
                                        <th>Nomor Kontak</th>
                                        <th>Link Video</th>
                                        <th>Deskiripsi Singkat</th>
                                        <th>Deskirpsi Lengkap</th>
                                        <th>Danasosial Id</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('dana_sosial_details/create')}}" class="btn btn-primary" role="button">Add dana_sosial_detail</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{url('dana_sosial_details/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/dana_sosial_details') }}/'+row[0]+'">'+data+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/dana_sosial_details') }}/'+row[0]+'/edit" class="btn btn-default">Update</a>';
                        },
                        "targets": 7                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 7+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ url('/dana_sosial_details') }}/' + id, type: 'DELETE'}).success(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection