@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Dana_sosial_detail</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Dana_sosial_detail    </div>

    <div class="panel-body">
                
        <form action="{{ url('/dana_sosial_details'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                                <div class="form-group">
                <label for="detail_id" class="col-sm-3 control-label">Detail Id</label>
                <div class="col-sm-6">
                    <input type="text" name="detail_id" id="detail_id" class="form-control" value="{{$model['detail_id'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="gambar_utama" class="col-sm-3 control-label">Gambar Utama</label>
                <div class="col-sm-6">
                    <input type="text" name="gambar_utama" id="gambar_utama" class="form-control" value="{{$model['gambar_utama'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="nomor_kontak" class="col-sm-3 control-label">Nomor Kontak</label>
                <div class="col-sm-6">
                    <input type="text" name="nomor_kontak" id="nomor_kontak" class="form-control" value="{{$model['nomor_kontak'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="link_video" class="col-sm-3 control-label">Link Video</label>
                <div class="col-sm-6">
                    <input type="text" name="link_video" id="link_video" class="form-control" value="{{$model['link_video'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="deskiripsi_singkat" class="col-sm-3 control-label">Deskiripsi Singkat</label>
                <div class="col-sm-6">
                    <input type="text" name="deskiripsi_singkat" id="deskiripsi_singkat" class="form-control" value="{{$model['deskiripsi_singkat'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="deskirpsi_lengkap" class="col-sm-3 control-label">Deskirpsi Lengkap</label>
                <div class="col-sm-6">
                    <input type="text" name="deskirpsi_lengkap" id="deskirpsi_lengkap" class="form-control" value="{{$model['deskirpsi_lengkap'] or ''}}">
                </div>
            </div>
                                                            <div class="form-group">
                <label for="danasosial_id" class="col-sm-3 control-label">Danasosial Id</label>
                <div class="col-sm-6">
                    <input type="text" name="danasosial_id" id="danasosial_id" class="form-control" value="{{$model['danasosial_id'] or ''}}">
                </div>
            </div>
                                                            
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/dana_sosial_details') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection