@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('dana_sosials') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('dana_sosials') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Danasosial Id</th>
                                        <th>Judul</th>
                                        <th>Created By</th>
                                        <th>Target Dana</th>
                                        <th>Deadline</th>
                                        <th>Alamat</th>
                                        <th>Status Danasosial</th>
                                        <th>Status</th>
                                        <th>Kategori Id</th>
                                        <th>User Id</th>
                                        <th>Detail Id</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('dana_sosials/create')}}" class="btn btn-primary" role="button">Add dana_sosial</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{url('dana_sosials/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/dana_sosials') }}/'+row[0]+'">'+data+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/dana_sosials') }}/'+row[0]+'/edit" class="btn btn-default">Update</a>';
                        },
                        "targets": 11                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 11+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ url('/dana_sosials') }}/' + id, type: 'DELETE'}).success(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection