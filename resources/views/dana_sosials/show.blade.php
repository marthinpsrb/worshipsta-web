@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Dana_sosial</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Dana_sosial    </div>

    <div class="panel-body">
                

        <form action="{{ url('/dana_sosials') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="danasosial_id" class="col-sm-3 control-label">Danasosial Id</label>
            <div class="col-sm-6">
                <input type="text" name="danasosial_id" id="danasosial_id" class="form-control" value="{{$model['danasosial_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="judul" class="col-sm-3 control-label">Judul</label>
            <div class="col-sm-6">
                <input type="text" name="judul" id="judul" class="form-control" value="{{$model['judul'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="target_dana" class="col-sm-3 control-label">Target Dana</label>
            <div class="col-sm-6">
                <input type="text" name="target_dana" id="target_dana" class="form-control" value="{{$model['target_dana'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="deadline" class="col-sm-3 control-label">Deadline</label>
            <div class="col-sm-6">
                <input type="text" name="deadline" id="deadline" class="form-control" value="{{$model['deadline'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="alamat" class="col-sm-3 control-label">Alamat</label>
            <div class="col-sm-6">
                <input type="text" name="alamat" id="alamat" class="form-control" value="{{$model['alamat'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status_danasosial" class="col-sm-3 control-label">Status Danasosial</label>
            <div class="col-sm-6">
                <input type="text" name="status_danasosial" id="status_danasosial" class="form-control" value="{{$model['status_danasosial'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="kategori_id" class="col-sm-3 control-label">Kategori Id</label>
            <div class="col-sm-6">
                <input type="text" name="kategori_id" id="kategori_id" class="form-control" value="{{$model['kategori_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="user_id" class="col-sm-3 control-label">User Id</label>
            <div class="col-sm-6">
                <input type="text" name="user_id" id="user_id" class="form-control" value="{{$model['user_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="detail_id" class="col-sm-3 control-label">Detail Id</label>
            <div class="col-sm-6">
                <input type="text" name="detail_id" id="detail_id" class="form-control" value="{{$model['detail_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/dana_sosials') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection