@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Kategori_gereja</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Kategori_gereja    </div>

    <div class="panel-body">
                

        <form action="{{ url('/kategori_gerejas') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="kategorigereja_id" class="col-sm-3 control-label">Kategorigereja Id</label>
            <div class="col-sm-6">
                <input type="text" name="kategorigereja_id" id="kategorigereja_id" class="form-control" value="{{$model['kategorigereja_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="gereja" class="col-sm-3 control-label">Gereja</label>
            <div class="col-sm-6">
                <input type="text" name="gereja" id="gereja" class="form-control" value="{{$model['gereja'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="keterangan" class="col-sm-3 control-label">Keterangan</label>
            <div class="col-sm-6">
                <input type="text" name="keterangan" id="keterangan" class="form-control" value="{{$model['keterangan'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="urutan_ibadah" class="col-sm-3 control-label">Urutan Ibadah</label>
            <div class="col-sm-6">
                <input type="text" name="urutan_ibadah" id="urutan_ibadah" class="form-control" value="{{$model['urutan_ibadah'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="urutan_ibadahpersekutuan" class="col-sm-3 control-label">Urutan Ibadahpersekutuan</label>
            <div class="col-sm-6">
                <input type="text" name="urutan_ibadahpersekutuan" id="urutan_ibadahpersekutuan" class="form-control" value="{{$model['urutan_ibadahpersekutuan'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/kategori_gerejas') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection