@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Cms_modul</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Cms_modul    </div>

    <div class="panel-body">
                
        <form action="{{ url('/cms_moduls'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                    <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
                                                                                                                                                <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Created At</label>
                <div class="col-sm-6">
                    <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-6">
                    <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}">
                </div>
            </div>
                                                            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{$model['name'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="icon" class="col-sm-3 control-label">Icon</label>
                <div class="col-sm-6">
                    <input type="text" name="icon" id="icon" class="form-control" value="{{$model['icon'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="path" class="col-sm-3 control-label">Path</label>
                <div class="col-sm-6">
                    <input type="text" name="path" id="path" class="form-control" value="{{$model['path'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="table_name" class="col-sm-3 control-label">Table Name</label>
                <div class="col-sm-6">
                    <input type="text" name="table_name" id="table_name" class="form-control" value="{{$model['table_name'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="controller" class="col-sm-3 control-label">Controller</label>
                <div class="col-sm-6">
                    <input type="text" name="controller" id="controller" class="form-control" value="{{$model['controller'] or ''}}">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="is_protected" class="col-sm-3 control-label">Is Protected</label>
                <div class="col-sm-2">
                    <input type="number" name="is_protected" id="is_protected" class="form-control" value="{{$model['is_protected'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="is_active" class="col-sm-3 control-label">Is Active</label>
                <div class="col-sm-2">
                    <input type="number" name="is_active" id="is_active" class="form-control" value="{{$model['is_active'] or ''}}">
                </div>
            </div>
                                                
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/cms_moduls') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection