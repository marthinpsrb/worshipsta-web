@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Role</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Role    </div>

    <div class="panel-body">
                
        <form action="{{ url('/roles'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                                <div class="form-group">
                <label for="role_id" class="col-sm-3 control-label">Role Id</label>
                <div class="col-sm-6">
                    <input type="text" name="role_id" id="role_id" class="form-control" value="{{$model['role_id'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="nama_role" class="col-sm-3 control-label">Nama Role</label>
                <div class="col-sm-6">
                    <input type="text" name="nama_role" id="nama_role" class="form-control" value="{{$model['nama_role'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="keterangan" class="col-sm-3 control-label">Keterangan</label>
                <div class="col-sm-6">
                    <input type="text" name="keterangan" id="keterangan" class="form-control" value="{{$model['keterangan'] or ''}}">
                </div>
            </div>
                                                                        <div class="form-group">
                <label for="status" class="col-sm-3 control-label">Status</label>
                <div class="col-sm-2">
                    <input type="number" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}">
                </div>
            </div>
                                                
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/roles') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection