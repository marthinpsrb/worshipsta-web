@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Gereja</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Gereja    </div>

    <div class="panel-body">
                
        <form action="{{ url('/gerejas'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                                <div class="form-group">
                <label for="gereja_id" class="col-sm-3 control-label">Gereja Id</label>
                <div class="col-sm-6">
                    <input type="text" name="gereja_id" id="gereja_id" class="form-control" value="{{$model['gereja_id'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="nama_gereja" class="col-sm-3 control-label">Nama Gereja</label>
                <div class="col-sm-6">
                    <input type="text" name="nama_gereja" id="nama_gereja" class="form-control" value="{{$model['nama_gereja'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="foto_gereja" class="col-sm-3 control-label">Foto Gereja</label>
                <div class="col-sm-6">
                    <input type="text" name="foto_gereja" id="foto_gereja" class="form-control" value="{{$model['foto_gereja'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="sejarah_gereja" class="col-sm-3 control-label">Sejarah Gereja</label>
                <div class="col-sm-6">
                    <input type="text" name="sejarah_gereja" id="sejarah_gereja" class="form-control" value="{{$model['sejarah_gereja'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                <div class="col-sm-6">
                    <input type="text" name="alamat" id="alamat" class="form-control" value="{{$model['alamat'] or ''}}">
                </div>
            </div>
                                                                        <div class="form-group">
                <label for="latitude" class="col-sm-3 control-label">Latitude</label>
                <div class="col-sm-2">
                    <input type="number" name="latitude" id="latitude" class="form-control" value="{{$model['latitude'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="langitude" class="col-sm-3 control-label">Langitude</label>
                <div class="col-sm-2">
                    <input type="number" name="langitude" id="langitude" class="form-control" value="{{$model['langitude'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="status" class="col-sm-3 control-label">Status</label>
                <div class="col-sm-2">
                    <input type="number" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="created_by" class="col-sm-3 control-label">Created By</label>
                <div class="col-sm-6">
                    <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}">
                </div>
            </div>
                                                                                                                        <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-3">
                    <input type="date" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}">
                </div>
            </div>
                                                                        <div class="form-group">
                <label for="kategorigereja_id" class="col-sm-3 control-label">Kategorigereja Id</label>
                <div class="col-sm-6">
                    <input type="text" name="kategorigereja_id" id="kategorigereja_id" class="form-control" value="{{$model['kategorigereja_id'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="user_id" class="col-sm-3 control-label">User Id</label>
                <div class="col-sm-6">
                    <input type="text" name="user_id" id="user_id" class="form-control" value="{{$model['user_id'] or ''}}">
                </div>
            </div>
                                                            
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/gerejas') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection