@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('gerejas') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('gerejas') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Gereja Id</th>
                                        <th>Nama Gereja</th>
                                        <th>Foto Gereja</th>
                                        <th>Sejarah Gereja</th>
                                        <th>Alamat</th>
                                        <th>Latitude</th>
                                        <th>Langitude</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th>Updated At</th>
                                        <th>Kategorigereja Id</th>
                                        <th>User Id</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('gerejas/create')}}" class="btn btn-primary" role="button">Add gereja</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{url('gerejas/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/gerejas') }}/'+row[0]+'">'+data+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/gerejas') }}/'+row[0]+'/edit" class="btn btn-default">Update</a>';
                        },
                        "targets": 12                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 12+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ url('/gerejas') }}/' + id, type: 'DELETE'}).success(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection