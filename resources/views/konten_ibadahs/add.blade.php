@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Konten_ibadah</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Konten_ibadah    </div>

    <div class="panel-body">
                
        <form action="{{ url('/konten_ibadahs'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                                <div class="form-group">
                <label for="konten_id" class="col-sm-3 control-label">Konten Id</label>
                <div class="col-sm-6">
                    <input type="text" name="konten_id" id="konten_id" class="form-control" value="{{$model['konten_id'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="konten" class="col-sm-3 control-label">Konten</label>
                <div class="col-sm-6">
                    <input type="text" name="konten" id="konten" class="form-control" value="{{$model['konten'] or ''}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="waktu" class="col-sm-3 control-label">Waktu</label>
                <div class="col-sm-3">
                    <input type="date" name="waktu" id="waktu" class="form-control" value="{{$model['waktu'] or ''}}">
                </div>
            </div>
                                                                        <div class="form-group">
                <label for="link_download" class="col-sm-3 control-label">Link Download</label>
                <div class="col-sm-6">
                    <input type="text" name="link_download" id="link_download" class="form-control" value="{{$model['link_download'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="created_by" class="col-sm-3 control-label">Created By</label>
                <div class="col-sm-6">
                    <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="status" class="col-sm-3 control-label">Status</label>
                <div class="col-sm-2">
                    <input type="number" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="tataibadah_id" class="col-sm-3 control-label">Tataibadah Id</label>
                <div class="col-sm-6">
                    <input type="text" name="tataibadah_id" id="tataibadah_id" class="form-control" value="{{$model['tataibadah_id'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="jadwal_id" class="col-sm-3 control-label">Jadwal Id</label>
                <div class="col-sm-6">
                    <input type="text" name="jadwal_id" id="jadwal_id" class="form-control" value="{{$model['jadwal_id'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="jenisibadah_id" class="col-sm-3 control-label">Jenisibadah Id</label>
                <div class="col-sm-6">
                    <input type="text" name="jenisibadah_id" id="jenisibadah_id" class="form-control" value="{{$model['jenisibadah_id'] or ''}}">
                </div>
            </div>
                                                            
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/konten_ibadahs') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection