@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Cms_email_queue</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Cms_email_queue    </div>

    <div class="panel-body">
                
        <form action="{{ url('/cms_email_queues'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                    <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
                                                                                                                                                <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Created At</label>
                <div class="col-sm-6">
                    <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-6">
                    <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="send_at" class="col-sm-3 control-label">Send At</label>
                <div class="col-sm-3">
                    <input type="date" name="send_at" id="send_at" class="form-control" value="{{$model['send_at'] or ''}}">
                </div>
            </div>
                                                                        <div class="form-group">
                <label for="email_recipient" class="col-sm-3 control-label">Email Recipient</label>
                <div class="col-sm-6">
                    <input type="text" name="email_recipient" id="email_recipient" class="form-control" value="{{$model['email_recipient'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="email_from_email" class="col-sm-3 control-label">Email From Email</label>
                <div class="col-sm-6">
                    <input type="text" name="email_from_email" id="email_from_email" class="form-control" value="{{$model['email_from_email'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="email_from_name" class="col-sm-3 control-label">Email From Name</label>
                <div class="col-sm-6">
                    <input type="text" name="email_from_name" id="email_from_name" class="form-control" value="{{$model['email_from_name'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="email_cc_email" class="col-sm-3 control-label">Email Cc Email</label>
                <div class="col-sm-6">
                    <input type="text" name="email_cc_email" id="email_cc_email" class="form-control" value="{{$model['email_cc_email'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="email_subject" class="col-sm-3 control-label">Email Subject</label>
                <div class="col-sm-6">
                    <input type="text" name="email_subject" id="email_subject" class="form-control" value="{{$model['email_subject'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="email_content" class="col-sm-3 control-label">Email Content</label>
                <div class="col-sm-6">
                    <input type="text" name="email_content" id="email_content" class="form-control" value="{{$model['email_content'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="email_attachments" class="col-sm-3 control-label">Email Attachments</label>
                <div class="col-sm-6">
                    <input type="text" name="email_attachments" id="email_attachments" class="form-control" value="{{$model['email_attachments'] or ''}}">
                </div>
            </div>
                                                                        <div class="form-group">
                <label for="is_sent" class="col-sm-3 control-label">Is Sent</label>
                <div class="col-sm-2">
                    <input type="number" name="is_sent" id="is_sent" class="form-control" value="{{$model['is_sent'] or ''}}">
                </div>
            </div>
                                                
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/cms_email_queues') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection