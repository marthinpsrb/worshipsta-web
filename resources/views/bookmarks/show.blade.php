@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Bookmark</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Bookmark    </div>

    <div class="panel-body">
                

        <form action="{{ url('/bookmarks') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="bookmark_id" class="col-sm-3 control-label">Bookmark Id</label>
            <div class="col-sm-6">
                <input type="text" name="bookmark_id" id="bookmark_id" class="form-control" value="{{$model['bookmark_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="isi_bookmark" class="col-sm-3 control-label">Isi Bookmark</label>
            <div class="col-sm-6">
                <input type="text" name="isi_bookmark" id="isi_bookmark" class="form-control" value="{{$model['isi_bookmark'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Created At</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="versi_id" class="col-sm-3 control-label">Versi Id</label>
            <div class="col-sm-6">
                <input type="text" name="versi_id" id="versi_id" class="form-control" value="{{$model['versi_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/bookmarks') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection