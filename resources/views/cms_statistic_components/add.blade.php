@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Cms_statistic_component</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Cms_statistic_component    </div>

    <div class="panel-body">
                
        <form action="{{ url('/cms_statistic_components'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                    <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
                                                                                                                        <div class="form-group">
                <label for="id_cms_statistics" class="col-sm-3 control-label">Id Cms Statistics</label>
                <div class="col-sm-2">
                    <input type="number" name="id_cms_statistics" id="id_cms_statistics" class="form-control" value="{{$model['id_cms_statistics'] or ''}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="componentID" class="col-sm-3 control-label">ComponentID</label>
                <div class="col-sm-6">
                    <input type="text" name="componentID" id="componentID" class="form-control" value="{{$model['componentID'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="component_name" class="col-sm-3 control-label">Component Name</label>
                <div class="col-sm-6">
                    <input type="text" name="component_name" id="component_name" class="form-control" value="{{$model['component_name'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="area_name" class="col-sm-3 control-label">Area Name</label>
                <div class="col-sm-6">
                    <input type="text" name="area_name" id="area_name" class="form-control" value="{{$model['area_name'] or ''}}">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="sorting" class="col-sm-3 control-label">Sorting</label>
                <div class="col-sm-2">
                    <input type="number" name="sorting" id="sorting" class="form-control" value="{{$model['sorting'] or ''}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{$model['name'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="config" class="col-sm-3 control-label">Config</label>
                <div class="col-sm-6">
                    <input type="text" name="config" id="config" class="form-control" value="{{$model['config'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Created At</label>
                <div class="col-sm-6">
                    <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-6">
                    <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}">
                </div>
            </div>
                        
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/cms_statistic_components') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection