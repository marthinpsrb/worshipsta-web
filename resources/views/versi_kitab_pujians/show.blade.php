@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Versi_kitab_pujian</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Versi_kitab_pujian    </div>

    <div class="panel-body">
                

        <form action="{{ url('/versi_kitab_pujians') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="versi_id" class="col-sm-3 control-label">Versi Id</label>
            <div class="col-sm-6">
                <input type="text" name="versi_id" id="versi_id" class="form-control" value="{{$model['versi_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nama_versi" class="col-sm-3 control-label">Nama Versi</label>
            <div class="col-sm-6">
                <input type="text" name="nama_versi" id="nama_versi" class="form-control" value="{{$model['nama_versi'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="kitabpujian_id" class="col-sm-3 control-label">Kitabpujian Id</label>
            <div class="col-sm-6">
                <input type="text" name="kitabpujian_id" id="kitabpujian_id" class="form-control" value="{{$model['kitabpujian_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/versi_kitab_pujians') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection