@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Cms_email_template</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Cms_email_template    </div>

    <div class="panel-body">
                
        <form action="{{ url('/cms_email_templates'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                    <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{$model['name'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="slug" class="col-sm-3 control-label">Slug</label>
                <div class="col-sm-6">
                    <input type="text" name="slug" id="slug" class="form-control" value="{{$model['slug'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="subject" class="col-sm-3 control-label">Subject</label>
                <div class="col-sm-6">
                    <input type="text" name="subject" id="subject" class="form-control" value="{{$model['subject'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="content" class="col-sm-3 control-label">Content</label>
                <div class="col-sm-6">
                    <input type="text" name="content" id="content" class="form-control" value="{{$model['content'] or ''}}">
                </div>
            </div>
                                                            <div class="form-group">
                <label for="description" class="col-sm-3 control-label">Description</label>
                <div class="col-sm-6">
                    <input type="text" name="description" id="description" class="form-control" value="{{$model['description'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="from_name" class="col-sm-3 control-label">From Name</label>
                <div class="col-sm-6">
                    <input type="text" name="from_name" id="from_name" class="form-control" value="{{$model['from_name'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="from_email" class="col-sm-3 control-label">From Email</label>
                <div class="col-sm-6">
                    <input type="text" name="from_email" id="from_email" class="form-control" value="{{$model['from_email'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="cc_email" class="col-sm-3 control-label">Cc Email</label>
                <div class="col-sm-6">
                    <input type="text" name="cc_email" id="cc_email" class="form-control" value="{{$model['cc_email'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Created At</label>
                <div class="col-sm-6">
                    <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-6">
                    <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}">
                </div>
            </div>
                        
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/cms_email_templates') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection