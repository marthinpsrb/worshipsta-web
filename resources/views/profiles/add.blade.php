@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Profile</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Profile    </div>

    <div class="panel-body">
                
        <form action="{{ url('/profiles'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                                <div class="form-group">
                <label for="profile_id" class="col-sm-3 control-label">Profile Id</label>
                <div class="col-sm-6">
                    <input type="text" name="profile_id" id="profile_id" class="form-control" value="{{$model['profile_id'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="nama_lengkap" class="col-sm-3 control-label">Nama Lengkap</label>
                <div class="col-sm-6">
                    <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" value="{{$model['nama_lengkap'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="nomor_identitas" class="col-sm-3 control-label">Nomor Identitas</label>
                <div class="col-sm-6">
                    <input type="text" name="nomor_identitas" id="nomor_identitas" class="form-control" value="{{$model['nomor_identitas'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="jemaat_gereja" class="col-sm-3 control-label">Jemaat Gereja</label>
                <div class="col-sm-6">
                    <input type="text" name="jemaat_gereja" id="jemaat_gereja" class="form-control" value="{{$model['jemaat_gereja'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="foto_gereja" class="col-sm-3 control-label">Foto Gereja</label>
                <div class="col-sm-6">
                    <input type="text" name="foto_gereja" id="foto_gereja" class="form-control" value="{{$model['foto_gereja'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="created_by" class="col-sm-3 control-label">Created By</label>
                <div class="col-sm-6">
                    <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}">
                </div>
            </div>
                                                                                                                        <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-3">
                    <input type="date" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}">
                </div>
            </div>
                                    
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/profiles') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection