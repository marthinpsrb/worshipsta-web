@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Cms_privileges_role</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Cms_privileges_role    </div>

    <div class="panel-body">
                

        <form action="{{ url('/cms_privileges_roles') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="id" class="col-sm-3 control-label">Id</label>
            <div class="col-sm-6">
                <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Created At</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
            <div class="col-sm-6">
                <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="is_visible" class="col-sm-3 control-label">Is Visible</label>
            <div class="col-sm-6">
                <input type="text" name="is_visible" id="is_visible" class="form-control" value="{{$model['is_visible'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="is_create" class="col-sm-3 control-label">Is Create</label>
            <div class="col-sm-6">
                <input type="text" name="is_create" id="is_create" class="form-control" value="{{$model['is_create'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="is_read" class="col-sm-3 control-label">Is Read</label>
            <div class="col-sm-6">
                <input type="text" name="is_read" id="is_read" class="form-control" value="{{$model['is_read'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="is_edit" class="col-sm-3 control-label">Is Edit</label>
            <div class="col-sm-6">
                <input type="text" name="is_edit" id="is_edit" class="form-control" value="{{$model['is_edit'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="is_delete" class="col-sm-3 control-label">Is Delete</label>
            <div class="col-sm-6">
                <input type="text" name="is_delete" id="is_delete" class="form-control" value="{{$model['is_delete'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="id_cms_privileges" class="col-sm-3 control-label">Id Cms Privileges</label>
            <div class="col-sm-6">
                <input type="text" name="id_cms_privileges" id="id_cms_privileges" class="form-control" value="{{$model['id_cms_privileges'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="id_cms_moduls" class="col-sm-3 control-label">Id Cms Moduls</label>
            <div class="col-sm-6">
                <input type="text" name="id_cms_moduls" id="id_cms_moduls" class="form-control" value="{{$model['id_cms_moduls'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/cms_privileges_roles') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection