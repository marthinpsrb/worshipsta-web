@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Cms_log</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Cms_log    </div>

    <div class="panel-body">
                

        <form action="{{ url('/cms_logs') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="id" class="col-sm-3 control-label">Id</label>
            <div class="col-sm-6">
                <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Created At</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
            <div class="col-sm-6">
                <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="ipaddress" class="col-sm-3 control-label">Ipaddress</label>
            <div class="col-sm-6">
                <input type="text" name="ipaddress" id="ipaddress" class="form-control" value="{{$model['ipaddress'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="useragent" class="col-sm-3 control-label">Useragent</label>
            <div class="col-sm-6">
                <input type="text" name="useragent" id="useragent" class="form-control" value="{{$model['useragent'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="url" class="col-sm-3 control-label">Url</label>
            <div class="col-sm-6">
                <input type="text" name="url" id="url" class="form-control" value="{{$model['url'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="description" class="col-sm-3 control-label">Description</label>
            <div class="col-sm-6">
                <input type="text" name="description" id="description" class="form-control" value="{{$model['description'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="id_cms_users" class="col-sm-3 control-label">Id Cms Users</label>
            <div class="col-sm-6">
                <input type="text" name="id_cms_users" id="id_cms_users" class="form-control" value="{{$model['id_cms_users'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/cms_logs') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection