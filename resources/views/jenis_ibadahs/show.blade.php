@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Jenis_ibadah</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Jenis_ibadah    </div>

    <div class="panel-body">
                

        <form action="{{ url('/jenis_ibadahs') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="jenisibadah_id" class="col-sm-3 control-label">Jenisibadah Id</label>
            <div class="col-sm-6">
                <input type="text" name="jenisibadah_id" id="jenisibadah_id" class="form-control" value="{{$model['jenisibadah_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nama_ibadah" class="col-sm-3 control-label">Nama Ibadah</label>
            <div class="col-sm-6">
                <input type="text" name="nama_ibadah" id="nama_ibadah" class="form-control" value="{{$model['nama_ibadah'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="keterangan" class="col-sm-3 control-label">Keterangan</label>
            <div class="col-sm-6">
                <input type="text" name="keterangan" id="keterangan" class="form-control" value="{{$model['keterangan'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/jenis_ibadahs') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection