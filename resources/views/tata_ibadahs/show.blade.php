@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Tata_ibadah</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Tata_ibadah    </div>

    <div class="panel-body">
                

        <form action="{{ url('/tata_ibadahs') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="tataibadah _id" class="col-sm-3 control-label">Tataibadah  Id</label>
            <div class="col-sm-6">
                <input type="text" name="tataibadah _id" id="tataibadah _id" class="form-control" value="{{$model['tataibadah _id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nama_tata" class="col-sm-3 control-label">Nama Tata</label>
            <div class="col-sm-6">
                <input type="text" name="nama_tata" id="nama_tata" class="form-control" value="{{$model['nama_tata'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="keterangan" class="col-sm-3 control-label">Keterangan</label>
            <div class="col-sm-6">
                <input type="text" name="keterangan" id="keterangan" class="form-control" value="{{$model['keterangan'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/tata_ibadahs') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection