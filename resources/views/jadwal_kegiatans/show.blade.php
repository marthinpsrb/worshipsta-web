@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Jadwal_kegiatan</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Jadwal_kegiatan    </div>

    <div class="panel-body">
                

        <form action="{{ url('/jadwal_kegiatans') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="jadwal_id" class="col-sm-3 control-label">Jadwal Id</label>
            <div class="col-sm-6">
                <input type="text" name="jadwal_id" id="jadwal_id" class="form-control" value="{{$model['jadwal_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nama_jadwal" class="col-sm-3 control-label">Nama Jadwal</label>
            <div class="col-sm-6">
                <input type="text" name="nama_jadwal" id="nama_jadwal" class="form-control" value="{{$model['nama_jadwal'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="keterangan" class="col-sm-3 control-label">Keterangan</label>
            <div class="col-sm-6">
                <input type="text" name="keterangan" id="keterangan" class="form-control" value="{{$model['keterangan'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="waktu" class="col-sm-3 control-label">Waktu</label>
            <div class="col-sm-6">
                <input type="text" name="waktu" id="waktu" class="form-control" value="{{$model['waktu'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="konten_id" class="col-sm-3 control-label">Konten Id</label>
            <div class="col-sm-6">
                <input type="text" name="konten_id" id="konten_id" class="form-control" value="{{$model['konten_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="gereja_id" class="col-sm-3 control-label">Gereja Id</label>
            <div class="col-sm-6">
                <input type="text" name="gereja_id" id="gereja_id" class="form-control" value="{{$model['gereja_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/jadwal_kegiatans') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection