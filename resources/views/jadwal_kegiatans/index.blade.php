@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('jadwal_kegiatans') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('jadwal_kegiatans') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Jadwal Id</th>
                                        <th>Nama Jadwal</th>
                                        <th>Keterangan</th>
                                        <th>Waktu</th>
                                        <th>Created By</th>
                                        <th>Status</th>
                                        <th>Konten Id</th>
                                        <th>Gereja Id</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('jadwal_kegiatans/create')}}" class="btn btn-primary" role="button">Add jadwal_kegiatan</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{url('jadwal_kegiatans/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/jadwal_kegiatans') }}/'+row[0]+'">'+data+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/jadwal_kegiatans') }}/'+row[0]+'/edit" class="btn btn-default">Update</a>';
                        },
                        "targets": 8                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 8+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ url('/jadwal_kegiatans') }}/' + id, type: 'DELETE'}).success(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection