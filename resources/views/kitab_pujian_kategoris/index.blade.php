@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('kitab_pujian_kategoris') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('kitab_pujian_kategoris') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Kitabpujian Id</th>
                                        <th>Nama Kategori</th>
                                        <th>Keterangan</th>
                                        <th>Created By</th>
                                        <th>Status</th>
                                        <th>Versi Id</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('kitab_pujian_kategoris/create')}}" class="btn btn-primary" role="button">Add kitab_pujian_kategori</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{url('kitab_pujian_kategoris/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/kitab_pujian_kategoris') }}/'+row[0]+'">'+data+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/kitab_pujian_kategoris') }}/'+row[0]+'/edit" class="btn btn-default">Update</a>';
                        },
                        "targets": 6                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 6+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ url('/kitab_pujian_kategoris') }}/' + id, type: 'DELETE'}).success(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection