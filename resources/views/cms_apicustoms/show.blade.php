@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Cms_apicustom</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Cms_apicustom    </div>

    <div class="panel-body">
                

        <form action="{{ url('/cms_apicustoms') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="id" class="col-sm-3 control-label">Id</label>
            <div class="col-sm-6">
                <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Created At</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
            <div class="col-sm-6">
                <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="permalink" class="col-sm-3 control-label">Permalink</label>
            <div class="col-sm-6">
                <input type="text" name="permalink" id="permalink" class="form-control" value="{{$model['permalink'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="tabel" class="col-sm-3 control-label">Tabel</label>
            <div class="col-sm-6">
                <input type="text" name="tabel" id="tabel" class="form-control" value="{{$model['tabel'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="aksi" class="col-sm-3 control-label">Aksi</label>
            <div class="col-sm-6">
                <input type="text" name="aksi" id="aksi" class="form-control" value="{{$model['aksi'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="kolom" class="col-sm-3 control-label">Kolom</label>
            <div class="col-sm-6">
                <input type="text" name="kolom" id="kolom" class="form-control" value="{{$model['kolom'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="orderby" class="col-sm-3 control-label">Orderby</label>
            <div class="col-sm-6">
                <input type="text" name="orderby" id="orderby" class="form-control" value="{{$model['orderby'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="sub_query_1" class="col-sm-3 control-label">Sub Query 1</label>
            <div class="col-sm-6">
                <input type="text" name="sub_query_1" id="sub_query_1" class="form-control" value="{{$model['sub_query_1'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="sql_where" class="col-sm-3 control-label">Sql Where</label>
            <div class="col-sm-6">
                <input type="text" name="sql_where" id="sql_where" class="form-control" value="{{$model['sql_where'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nama" class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-6">
                <input type="text" name="nama" id="nama" class="form-control" value="{{$model['nama'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="keterangan" class="col-sm-3 control-label">Keterangan</label>
            <div class="col-sm-6">
                <input type="text" name="keterangan" id="keterangan" class="form-control" value="{{$model['keterangan'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="parameter" class="col-sm-3 control-label">Parameter</label>
            <div class="col-sm-6">
                <input type="text" name="parameter" id="parameter" class="form-control" value="{{$model['parameter'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="method_type" class="col-sm-3 control-label">Method Type</label>
            <div class="col-sm-6">
                <input type="text" name="method_type" id="method_type" class="form-control" value="{{$model['method_type'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="parameters" class="col-sm-3 control-label">Parameters</label>
            <div class="col-sm-6">
                <input type="text" name="parameters" id="parameters" class="form-control" value="{{$model['parameters'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="responses" class="col-sm-3 control-label">Responses</label>
            <div class="col-sm-6">
                <input type="text" name="responses" id="responses" class="form-control" value="{{$model['responses'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/cms_apicustoms') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection