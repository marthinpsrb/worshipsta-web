@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Cms_setting</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Cms_setting    </div>

    <div class="panel-body">
                
        <form action="{{ url('/cms_settings'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                    <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
                                                                                                                                                <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Created At</label>
                <div class="col-sm-6">
                    <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-6">
                    <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}">
                </div>
            </div>
                                                            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{$model['name'] or ''}}">
                </div>
            </div>
                                                                                                                                    <div class="form-group">
                <label for="content" class="col-sm-3 control-label">Content</label>
                <div class="col-sm-6">
                    <input type="text" name="content" id="content" class="form-control" value="{{$model['content'] or ''}}">
                </div>
            </div>
                                                            <div class="form-group">
                <label for="content_input_type" class="col-sm-3 control-label">Content Input Type</label>
                <div class="col-sm-6">
                    <input type="text" name="content_input_type" id="content_input_type" class="form-control" value="{{$model['content_input_type'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="dataenum" class="col-sm-3 control-label">Dataenum</label>
                <div class="col-sm-6">
                    <input type="text" name="dataenum" id="dataenum" class="form-control" value="{{$model['dataenum'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="helper" class="col-sm-3 control-label">Helper</label>
                <div class="col-sm-6">
                    <input type="text" name="helper" id="helper" class="form-control" value="{{$model['helper'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="group_setting" class="col-sm-3 control-label">Group Setting</label>
                <div class="col-sm-6">
                    <input type="text" name="group_setting" id="group_setting" class="form-control" value="{{$model['group_setting'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="label" class="col-sm-3 control-label">Label</label>
                <div class="col-sm-6">
                    <input type="text" name="label" id="label" class="form-control" value="{{$model['label'] or ''}}">
                </div>
            </div>
                                                            
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/cms_settings') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection