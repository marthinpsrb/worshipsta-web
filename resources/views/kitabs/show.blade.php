@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Kitab</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Kitab    </div>

    <div class="panel-body">
                

        <form action="{{ url('/kitabs') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="kitab_id" class="col-sm-3 control-label">Kitab Id</label>
            <div class="col-sm-6">
                <input type="text" name="kitab_id" id="kitab_id" class="form-control" value="{{$model['kitab_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="kitab" class="col-sm-3 control-label">Kitab</label>
            <div class="col-sm-6">
                <input type="text" name="kitab" id="kitab" class="form-control" value="{{$model['kitab'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="pasal" class="col-sm-3 control-label">Pasal</label>
            <div class="col-sm-6">
                <input type="text" name="pasal" id="pasal" class="form-control" value="{{$model['pasal'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="ayat" class="col-sm-3 control-label">Ayat</label>
            <div class="col-sm-6">
                <input type="text" name="ayat" id="ayat" class="form-control" value="{{$model['ayat'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="firman" class="col-sm-3 control-label">Firman</label>
            <div class="col-sm-6">
                <input type="text" name="firman" id="firman" class="form-control" value="{{$model['firman'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="versi_id" class="col-sm-3 control-label">Versi Id</label>
            <div class="col-sm-6">
                <input type="text" name="versi_id" id="versi_id" class="form-control" value="{{$model['versi_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/kitabs') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection