@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Cms_privilege</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Cms_privilege    </div>

    <div class="panel-body">
                
        <form action="{{ url('/cms_privileges'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            @if (isset($model))
                <input type="hidden" name="_method" value="PATCH">
            @endif


                                    <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
                                                                                                                                                <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Created At</label>
                <div class="col-sm-6">
                    <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}">
                </div>
            </div>
                                                                                                <div class="form-group">
                <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
                <div class="col-sm-6">
                    <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}">
                </div>
            </div>
                                                            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{$model['name'] or ''}}">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="is_superadmin" class="col-sm-3 control-label">Is Superadmin</label>
                <div class="col-sm-2">
                    <input type="number" name="is_superadmin" id="is_superadmin" class="form-control" value="{{$model['is_superadmin'] or ''}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="theme_color" class="col-sm-3 control-label">Theme Color</label>
                <div class="col-sm-6">
                    <input type="text" name="theme_color" id="theme_color" class="form-control" value="{{$model['theme_color'] or ''}}">
                </div>
            </div>
                                                            
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/cms_privileges') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection