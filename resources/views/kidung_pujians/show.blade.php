@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Kidung_pujian</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Kidung_pujian    </div>

    <div class="panel-body">
                

        <form action="{{ url('/kidung_pujians') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="kidungpujian_id" class="col-sm-3 control-label">Kidungpujian Id</label>
            <div class="col-sm-6">
                <input type="text" name="kidungpujian_id" id="kidungpujian_id" class="form-control" value="{{$model['kidungpujian_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nomor" class="col-sm-3 control-label">Nomor</label>
            <div class="col-sm-6">
                <input type="text" name="nomor" id="nomor" class="form-control" value="{{$model['nomor'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nomor_notbalok" class="col-sm-3 control-label">Nomor Notbalok</label>
            <div class="col-sm-6">
                <input type="text" name="nomor_notbalok" id="nomor_notbalok" class="form-control" value="{{$model['nomor_notbalok'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="ayat" class="col-sm-3 control-label">Ayat</label>
            <div class="col-sm-6">
                <input type="text" name="ayat" id="ayat" class="form-control" value="{{$model['ayat'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="lagu" class="col-sm-3 control-label">Lagu</label>
            <div class="col-sm-6">
                <input type="text" name="lagu" id="lagu" class="form-control" value="{{$model['lagu'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="versi_id" class="col-sm-3 control-label">Versi Id</label>
            <div class="col-sm-6">
                <input type="text" name="versi_id" id="versi_id" class="form-control" value="{{$model['versi_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/kidung_pujians') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection