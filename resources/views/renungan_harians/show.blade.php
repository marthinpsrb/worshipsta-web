@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Renungan_harian</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Renungan_harian    </div>

    <div class="panel-body">
                

        <form action="{{ url('/renungan_harians') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="renungan_id" class="col-sm-3 control-label">Renungan Id</label>
            <div class="col-sm-6">
                <input type="text" name="renungan_id" id="renungan_id" class="form-control" value="{{$model['renungan_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="judul" class="col-sm-3 control-label">Judul</label>
            <div class="col-sm-6">
                <input type="text" name="judul" id="judul" class="form-control" value="{{$model['judul'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="ayat_renungan" class="col-sm-3 control-label">Ayat Renungan</label>
            <div class="col-sm-6">
                <input type="text" name="ayat_renungan" id="ayat_renungan" class="form-control" value="{{$model['ayat_renungan'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="isi_renungan" class="col-sm-3 control-label">Isi Renungan</label>
            <div class="col-sm-6">
                <input type="text" name="isi_renungan" id="isi_renungan" class="form-control" value="{{$model['isi_renungan'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Created At</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="rating" class="col-sm-3 control-label">Rating</label>
            <div class="col-sm-6">
                <input type="text" name="rating" id="rating" class="form-control" value="{{$model['rating'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="user_id" class="col-sm-3 control-label">User Id</label>
            <div class="col-sm-6">
                <input type="text" name="user_id" id="user_id" class="form-control" value="{{$model['user_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/renungan_harians') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection