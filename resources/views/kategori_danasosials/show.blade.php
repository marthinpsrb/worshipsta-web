@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Kategori_danasosial</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Kategori_danasosial    </div>

    <div class="panel-body">
                

        <form action="{{ url('/kategori_danasosials') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="kategori_id" class="col-sm-3 control-label">Kategori Id</label>
            <div class="col-sm-6">
                <input type="text" name="kategori_id" id="kategori_id" class="form-control" value="{{$model['kategori_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nama_kategori" class="col-sm-3 control-label">Nama Kategori</label>
            <div class="col-sm-6">
                <input type="text" name="nama_kategori" id="nama_kategori" class="form-control" value="{{$model['nama_kategori'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="deskripsi_kategori" class="col-sm-3 control-label">Deskripsi Kategori</label>
            <div class="col-sm-6">
                <input type="text" name="deskripsi_kategori" id="deskripsi_kategori" class="form-control" value="{{$model['deskripsi_kategori'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
            <div class="col-sm-6">
                <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/kategori_danasosials') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection