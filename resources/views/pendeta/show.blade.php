@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Pendetum</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Pendetum    </div>

    <div class="panel-body">
                

        <form action="{{ url('/pendeta') }}" method="POST" class="form-horizontal">


                
        <div class="form-group">
            <label for="pendeta_id" class="col-sm-3 control-label">Pendeta Id</label>
            <div class="col-sm-6">
                <input type="text" name="pendeta_id" id="pendeta_id" class="form-control" value="{{$model['pendeta_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="nama_pendeta" class="col-sm-3 control-label">Nama Pendeta</label>
            <div class="col-sm-6">
                <input type="text" name="nama_pendeta" id="nama_pendeta" class="form-control" value="{{$model['nama_pendeta'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-6">
                <input type="text" name="email" id="email" class="form-control" value="{{$model['email'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="foto_gereja" class="col-sm-3 control-label">Foto Gereja</label>
            <div class="col-sm-6">
                <input type="text" name="foto_gereja" id="foto_gereja" class="form-control" value="{{$model['foto_gereja'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="no_hp" class="col-sm-3 control-label">No Hp</label>
            <div class="col-sm-6">
                <input type="text" name="no_hp" id="no_hp" class="form-control" value="{{$model['no_hp'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="tahun_masuk" class="col-sm-3 control-label">Tahun Masuk</label>
            <div class="col-sm-6">
                <input type="text" name="tahun_masuk" id="tahun_masuk" class="form-control" value="{{$model['tahun_masuk'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <input type="text" name="status" id="status" class="form-control" value="{{$model['status'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_by" class="col-sm-3 control-label">Created By</label>
            <div class="col-sm-6">
                <input type="text" name="created_by" id="created_by" class="form-control" value="{{$model['created_by'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="user_id" class="col-sm-3 control-label">User Id</label>
            <div class="col-sm-6">
                <input type="text" name="user_id" id="user_id" class="form-control" value="{{$model['user_id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/pendeta') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>


        </form>

    </div>
</div>







@endsection