@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('pendeta') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('pendeta') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Pendeta Id</th>
                                        <th>Nama Pendeta</th>
                                        <th>Email</th>
                                        <th>Foto Gereja</th>
                                        <th>No Hp</th>
                                        <th>Tahun Masuk</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th>User Id</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('pendeta/create')}}" class="btn btn-primary" role="button">Add pendetum</a>
    </div>
</div>






@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,
                "ajax": "{{url('pendeta/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/pendeta') }}/'+row[0]+'">'+data+'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{ url('/pendeta') }}/'+row[0]+'/edit" class="btn btn-default">Update</a>';
                        },
                        "targets": 9                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 9+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax({ url: '{{ url('/pendeta') }}/' + id, type: 'DELETE'}).success(function() {
                theGrid.ajax.reload();
               });
            }
            return false;
        }
    </script>
@endsection