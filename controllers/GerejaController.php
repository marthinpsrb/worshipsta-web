<?php

namespace app\controllers;

use Yii;
use app\models\Gereja;
use app\models\GerejaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\KategoriGereja;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;

/**
 * GerejaController implements the CRUD actions for Gereja model.
 */
class GerejaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gereja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GerejaSearch();
        $dataGereja = Gereja::find()->all();

        //$model = $this->findModel($id);
        //$path = $model->foto_gereja;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataGereja' => $dataGereja,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gereja model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new GerejaSearch();
        $dataProvider = $searchModel->searchModel->search(Yii::$app->request->queryParams);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new Gereja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('create-gereja'))
            {        
                $model = new Gereja();
                
                $kategorigereja = \yii\helpers\ArrayHelper::map(KategoriGereja::find()->all(),
                        'id', 'gereja');

                if ($model->load(Yii::$app->request->post())) {

                $imageName = $model->nama_gereja; 
                var_dump($imageName);
                die;
                $model->foto_gereja = UploadedFile::getInstance($model, 'foto_gereja');

                $imageExtention = $model->foto_gereja->extension;
                $model->foto_gereja->saveAs('foto_gereja/'.$imageName.'.'.$imageExtention);
                $model->foto_gereja = ('foto_gereja/'.$imageName.'.'.$imageExtention);

                $model->status=1;
                $model->save();

                return $this->redirect(['index', 'id' => $model->id]);
                } else {
                    
                    return $this->render('create', [
                        'model' => $model,
                        'kategorigereja' => $kategorigereja,
                    ]);
                   }
        }
        else 
        {
            throw new ForbiddenHttpException ();
        }

    }

    /**
     * Updates an existing Gereja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $kategorigereja = \yii\helpers\ArrayHelper::map(KategoriGereja::find()->all(),
        'id', 'gereja');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'kategorigereja' => $kategorigereja,
            ]);
        }
    }

    /**
     * Deletes an existing Gereja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gereja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gereja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gereja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
