<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kategori_danasosial".
 *
 * @property integer $id
 * @property string $nama_kategori
 * @property string $deskripsi_kategori
 * @property string $created_by
 * @property string $updated_at
 * @property integer $status
 */
class KategoriDanasosial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori_danasosial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_kategori', 'deskripsi_kategori', 'created_by', 'updated_at'], 'required'],
            [['deskripsi_kategori'], 'string'],
            [['updated_at'], 'safe'],
            [['status'], 'integer'],
            [['nama_kategori'], 'string', 'max' => 35],
            [['created_by'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_kategori' => 'Nama Kategori',
            'deskripsi_kategori' => 'Deskripsi Kategori',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }
}
