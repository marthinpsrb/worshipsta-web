<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kitab".
 *
 * @property integer $id
 * @property string $kitab
 * @property string $kategori_kitab
 * @property integer $versi_kitab
 *
 * @property KitabVersion $versiKitab
 */
class Kitab extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kitab';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kitab', 'kategori_kitab'], 'required'],
            [['versi_kitab'], 'integer'],
            [['kitab', 'kategori_kitab'], 'string', 'max' => 255],
            [['versi_kitab'], 'exist', 'skipOnError' => true, 'targetClass' => KitabVersion::className(), 'targetAttribute' => ['versi_kitab' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kitab' => 'Kitab',
            'kategori_kitab' => 'Kategori Kitab',
            'versi_kitab' => 'Versi Kitab',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersiKitab()
    {
        return $this->hasOne(KitabVersion::className(), ['id' => 'versi_kitab']);
    }
}
