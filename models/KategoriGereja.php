<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kategori_gereja".
 *
 * @property integer $id
 * @property string $gereja
 * @property string $keterangan
 * @property string $urutan_ibadah
 * @property string $urutan_ibadahpersekutuan
 * @property string $created_by
 * @property integer $status
 *
 * @property Gereja[] $gerejas
 */
class KategoriGereja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori_gereja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gereja', 'keterangan', 'urutan_ibadah', 'urutan_ibadahpersekutuan', 'status'], 'required'],
            [['keterangan'], 'string'],
            [['status'], 'integer'],
            [['gereja'], 'string', 'max' => 45],
            [['urutan_ibadah', 'urutan_ibadahpersekutuan', 'created_by'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gereja' => 'Gereja',
            'keterangan' => 'Keterangan',
            'urutan_ibadah' => 'Urutan Ibadah',
            'urutan_ibadahpersekutuan' => 'Urutan Ibadahpersekutuan',
            'created_by' => 'Created By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGerejas()
    {
        return $this->hasMany(Gereja::className(), ['kategorigereja_id' => 'id']);
    }
}
