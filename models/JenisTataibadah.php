<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_tataibadah".
 *
 * @property integer $id_jenistataibadah
 * @property integer $id_jenisibadah
 * @property string $tema_ibadah
 * @property string $tanggal
 *
 * @property JenisIbadah $idJenisibadah
 * @property TataIbadah[] $tataIbadahs
 */
class JenisTataibadah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_tataibadah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_jenisibadah', 'tanggal'], 'required'],
            [['id_jenisibadah'], 'integer'],
            [['tema_ibadah'], 'string'],
            [['tanggal'], 'string', 'max' => 20],
            [['id_jenisibadah'], 'exist', 'skipOnError' => true, 'targetClass' => JenisIbadah::className(), 'targetAttribute' => ['id_jenisibadah' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_jenistataibadah' => 'Id Jenistataibadah',
            'id_jenisibadah' => 'Id Jenisibadah',
            'tema_ibadah' => 'Tema Ibadah',
            'tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenisibadah()
    {
        return $this->hasOne(JenisIbadah::className(), ['id' => 'id_jenisibadah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTataIbadahs()
    {
        return $this->hasMany(TataIbadah::className(), ['id_jenistataibadah' => 'id_jenistataibadah']);
    }
}
