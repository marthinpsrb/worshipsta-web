<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TataIbadah;

/**
 * TataIbadahSearch represents the model behind the search form about `app\models\TataIbadah`.
 */
class TataIbadahSearch extends TataIbadah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tataibadah', 'id_jenistataibadah', 'id_gereja', 'urutan_tataibadah'], 'integer'],
            [['judul_tataibadah', 'konten_tataibadah'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TataIbadah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tataibadah' => $this->id_tataibadah,
            'id_jenistataibadah' => $this->id_jenistataibadah,
            'id_gereja' => $this->id_gereja,
            'urutan_tataibadah' => $this->urutan_tataibadah,
        ]);

        $query->andFilterWhere(['like', 'judul_tataibadah', $this->judul_tataibadah])
            ->andFilterWhere(['like', 'konten_tataibadah', $this->konten_tataibadah]);

        return $dataProvider;
    }
}
