<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kitab_version".
 *
 * @property integer $id
 * @property string $nama_versi
 * @property string $keterangan
 *
 * @property Kitab[] $kitabs
 * @property KitabIsi[] $kitabIsis
 */
class KitabVersion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kitab_version';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_versi', 'keterangan'], 'required'],
            [['keterangan'], 'string'],
            [['nama_versi'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_versi' => 'Nama Versi',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKitabs()
    {
        return $this->hasMany(Kitab::className(), ['versi_kitab' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKitabIsis()
    {
        return $this->hasMany(KitabIsi::className(), ['versi_kitab' => 'id']);
    }
}
