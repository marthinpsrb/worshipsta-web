<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property integer $status
 * @property string $created_by
 * @property string $updated_at
 * @property string $last_login
 * @property string $authKey
 *
 * @property AuthenticationApi[] $authenticationApis
 * @property DanaSosial[] $danaSosials
 * @property Gereja[] $gerejas
 * @property Pendeta[] $pendetas
 * @property Profile[] $profiles
 * @property RenunganHarian[] $renunganHarians
 * @property TataIbadah[] $tataIbadahs
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'username', 'email', 'password', 'status', 'created_by', 'updated_at', 'last_login', 'authKey','password_hash'], 'required'],
            [['status'], 'integer'],
            [['updated_at', 'last_login'], 'safe'],
            [['id', 'authKey'], 'string', 'max' => 50],
            [['username'], 'string', 'max' => 40],
            [['email', 'password'], 'string', 'max' => 35],
            [['created_by'], 'string', 'max' => 30],
            [['password_hash'], 'string', 'max' => 52],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'last_login' => 'Last Login',
            'authKey' => 'Auth Key',
            'password_hash' => 'Password Hash',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthenticationApis()
    {
        return $this->hasMany(AuthenticationApi::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDanaSosials()
    {
        return $this->hasMany(DanaSosial::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGerejas()
    {
        return $this->hasMany(Gereja::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendetas()
    {
        return $this->hasMany(Pendeta::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRenunganHarians()
    {
        return $this->hasMany(RenunganHarian::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTataIbadahs()
    {
        return $this->hasMany(TataIbadah::className(), ['created_by' => 'id']);
    }

    public function getAuthKey() {
        return $this-> authKey;
    }

    public function getId() {
        return $this-> id;
    }

    public function validateAuthKey($authKey) {
        return $this-> authKey === $authKey;
    }

    public static function findIdentity($id) {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new \yii\base\NotSupportedException;
    }
    
    public static function findByUsername($username){
        return self::findOne(['username'=>$username]);
    } 
    
    public function validatePassword($password){
        return $this->password === $password;
    }
}
