<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kidung_version".
 *
 * @property integer $id
 * @property string $nama_versi
 * @property string $keterangan
 *
 * @property Kidung[] $kidungs
 * @property KidungIsi[] $kidungIsis
 */
class KidungVersion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kidung_version';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_versi', 'keterangan'], 'required'],
            [['keterangan'], 'string'],
            [['nama_versi'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_versi' => 'Nama Versi',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidungs()
    {
        return $this->hasMany(Kidung::className(), ['versi_kidung' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidungIsis()
    {
        return $this->hasMany(KidungIsi::className(), ['versi_kidung' => 'id']);
    }
}
