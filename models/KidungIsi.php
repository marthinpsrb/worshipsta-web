<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kidung_isi".
 *
 * @property integer $id
 * @property integer $nomor_kidungpujian
 * @property integer $ayat
 * @property string $isi
 * @property integer $versi_kidung
 *
 * @property KidungVersion $versiKidung
 */
class KidungIsi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kidung_isi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'nomor_kidungpujian', 'ayat', 'versi_kidung'], 'integer'],
            [['isi'], 'string', 'max' => 20000],
            [['versi_kidung'], 'exist', 'skipOnError' => true, 'targetClass' => KidungVersion::className(), 'targetAttribute' => ['versi_kidung' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor_kidungpujian' => 'Nomor Kidungpujian',
            'ayat' => 'Ayat',
            'isi' => 'Isi',
            'versi_kidung' => 'Versi Kidung',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersiKidung()
    {
        return $this->hasOne(KidungVersion::className(), ['id' => 'versi_kidung']);
    }
}
