<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_ibadah".
 *
 * @property integer $id
 * @property string $nama_ibadah
 * @property string $keterangan
 * @property integer $status
 * @property integer $gereja_id
 * @property resource $waktu
 *
 * @property JenisTataibadah[] $jenisTataibadahs
 * @property KontenIbadah[] $kontenIbadahs
 */
class JenisIbadah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_ibadah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_ibadah', 'keterangan', 'status', 'gereja_id', 'waktu'], 'required'],
            [['keterangan'], 'string'],
            [['status', 'gereja_id'], 'integer'],
            [['nama_ibadah'], 'string', 'max' => 45],
            [['waktu'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_ibadah' => 'Nama Ibadah',
            'keterangan' => 'Keterangan',
            'status' => 'Status',
            'gereja_id' => 'Gereja ID',
            'waktu' => 'Waktu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisTataibadahs()
    {
        return $this->hasMany(JenisTataibadah::className(), ['id_jenisibadah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKontenIbadahs()
    {
        return $this->hasMany(KontenIbadah::className(), ['jenisibadah_id' => 'id']);
    }
}
