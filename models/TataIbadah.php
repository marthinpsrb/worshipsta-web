<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tata_ibadah".
 *
 * @property integer $id_tataibadah
 * @property integer $id_jenistataibadah
 * @property integer $id_gereja
 * @property integer $urutan_tataibadah
 * @property string $judul_tataibadah
 * @property string $konten_tataibadah
 *
 * @property KontenIbadah[] $kontenIbadahs
 * @property JenisTataibadah $idJenistataibadah
 * @property Gereja $idGereja
 */
class TataIbadah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tata_ibadah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_jenistataibadah', 'id_gereja'], 'required'],
            [['id_jenistataibadah', 'id_gereja', 'urutan_tataibadah'], 'integer'],
            [['konten_tataibadah'], 'string'],
            [['judul_tataibadah'], 'string', 'max' => 40],
            [['id_jenistataibadah'], 'exist', 'skipOnError' => true, 'targetClass' => JenisTataibadah::className(), 'targetAttribute' => ['id_jenistataibadah' => 'id_jenistataibadah']],
            [['id_gereja'], 'exist', 'skipOnError' => true, 'targetClass' => Gereja::className(), 'targetAttribute' => ['id_gereja' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tataibadah' => 'Id Tataibadah',
            'id_jenistataibadah' => 'Id Jenistataibadah',
            'id_gereja' => 'Id Gereja',
            'urutan_tataibadah' => 'Urutan Tataibadah',
            'judul_tataibadah' => 'Judul Tataibadah',
            'konten_tataibadah' => 'Konten Tataibadah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKontenIbadahs()
    {
        return $this->hasMany(KontenIbadah::className(), ['tataibadah_id' => 'id_tataibadah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenistataibadah()
    {
        return $this->hasOne(JenisTataibadah::className(), ['id_jenistataibadah' => 'id_jenistataibadah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGereja()
    {
        return $this->hasOne(Gereja::className(), ['id' => 'id_gereja']);
    }
}
