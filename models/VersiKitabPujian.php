<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "versi_kitab_pujian".
 *
 * @property string $id
 * @property string $nama_versi
 * @property string $created_by
 * @property integer $status
 * @property string $kitabpujian_id
 *
 * @property Bookmark[] $bookmarks
 * @property KidungPujian[] $kidungPujians
 * @property Kitab[] $kitabs
 * @property KitabPujianKategori[] $kitabPujianKategoris
 * @property KitabPujianKategori $kitabpujian
 */
class VersiKitabPujian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'versi_kitab_pujian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_versi', 'created_by', 'status', 'kitabpujian_id'], 'required'],
            [['status'], 'integer'],
            [['id', 'created_by', 'kitabpujian_id'], 'string', 'max' => 30],
            [['nama_versi'], 'string', 'max' => 45],
            [['kitabpujian_id'], 'exist', 'skipOnError' => true, 'targetClass' => KitabPujianKategori::className(), 'targetAttribute' => ['kitabpujian_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_versi' => 'Nama Versi',
            'created_by' => 'Created By',
            'status' => 'Status',
            'kitabpujian_id' => 'Kitabpujian ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookmarks()
    {
        return $this->hasMany(Bookmark::className(), ['versi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKidungPujians()
    {
        return $this->hasMany(KidungPujian::className(), ['versi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKitabs()
    {
        return $this->hasMany(Kitab::className(), ['versi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKitabPujianKategoris()
    {
        return $this->hasMany(KitabPujianKategori::className(), ['versi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKitabpujian()
    {
        return $this->hasOne(KitabPujianKategori::className(), ['id' => 'kitabpujian_id']);
    }
}
