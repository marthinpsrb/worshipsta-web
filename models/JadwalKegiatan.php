<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jadwal_kegiatan".
 *
 * @property integer $id
 * @property string $nama_jadwal
 * @property string $keterangan
 * @property string $waktu
 * @property string $created_by
 * @property integer $gereja_id
 *
 * @property Gereja $gereja
 * @property KontenIbadah[] $kontenIbadahs
 */
class JadwalKegiatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jadwal_kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_jadwal', 'keterangan', 'waktu', 'created_by', 'gereja_id'], 'required'],
            [['keterangan'], 'string'],
            [['gereja_id'], 'integer'],
            [['nama_jadwal'], 'string', 'max' => 35],
            [['waktu', 'created_by'], 'string', 'max' => 30],
            [['gereja_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gereja::className(), 'targetAttribute' => ['gereja_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_jadwal' => 'Nama Jadwal',
            'keterangan' => 'Keterangan',
            'waktu' => 'Waktu',
            'created_by' => 'Created By',
            'gereja_id' => 'Gereja ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGereja()
    {
        return $this->hasOne(Gereja::className(), ['id' => 'gereja_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKontenIbadahs()
    {
        return $this->hasMany(KontenIbadah::className(), ['jadwal_id' => 'id']);
    }
}
