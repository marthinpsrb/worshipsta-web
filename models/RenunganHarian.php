<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "renungan_harian".
 *
 * @property integer $id
 * @property string $judul
 * @property string $ayat_renungan
 * @property string $isi_renungan
 * @property string $created_at
 * @property integer $status
 * @property string $user_id
 * @property string $sumber
 * @property string $gambar
 *
 * @property User $user
 */
class RenunganHarian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'renungan_harian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'ayat_renungan', 'isi_renungan', 'created_at', 'status', 'user_id', 'sumber', 'gambar'], 'required'],
            [['isi_renungan'], 'string'],
            [['created_at'], 'safe'],
            [['status'], 'integer'],
            [['judul', 'user_id'], 'string', 'max' => 35],
            [['ayat_renungan', 'gambar'], 'string', 'max' => 50],
            [['sumber'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'ayat_renungan' => 'Ayat Renungan',
            'isi_renungan' => 'Isi Renungan',
            'created_at' => 'Created At',
            'status' => 'Status',
            'user_id' => 'User ID',
            'sumber' => 'Sumber',
            'gambar' => 'Gambar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
