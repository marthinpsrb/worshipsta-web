<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JenisTataibadah;

/**
 * JenisTataibadahSearch represents the model behind the search form about `app\models\JenisTataibadah`.
 */
class JenisTataibadahSearch extends JenisTataibadah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_jenistataibadah', 'id_jenisibadah'], 'integer'],
            [['tema_ibadah', 'tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JenisTataibadah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jenistataibadah' => $this->id_jenistataibadah,
            'id_jenisibadah' => $this->id_jenisibadah,
        ]);

        $query->andFilterWhere(['like', 'tema_ibadah', $this->tema_ibadah])
            ->andFilterWhere(['like', 'tanggal', $this->tanggal]);

        return $dataProvider;
    }
}
