<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pendeta".
 *
 * @property integer $id
 * @property string $nama_pendeta
 * @property string $email
 * @property string $foto_gereja
 * @property string $no_hp
 * @property string $tahun_masuk
 * @property integer $status
 * @property string $created_by
 * @property string $user_id
 *
 * @property User $user
 */
class Pendeta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pendeta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_pendeta', 'email', 'foto_gereja', 'no_hp', 'tahun_masuk', 'status', 'created_by', 'user_id'], 'required'],
            [['status'], 'integer'],
            [['nama_pendeta', 'email'], 'string', 'max' => 35],
            [['foto_gereja', 'created_by', 'user_id'], 'string', 'max' => 30],
            [['no_hp'], 'string', 'max' => 13],
            [['tahun_masuk'], 'string', 'max' => 4],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pendeta' => 'Nama Pendeta',
            'email' => 'Email',
            'foto_gereja' => 'Foto Gereja',
            'no_hp' => 'No Hp',
            'tahun_masuk' => 'Tahun Masuk',
            'status' => 'Status',
            'created_by' => 'Created By',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
