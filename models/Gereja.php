<?php

namespace app\models;

use Yii;
use app\controllers\GerejaControllers;
use app\models\KategoriGereja;

/**
 * This is the model class for table "gereja".
 *
 * @property integer $id
 * @property string $nama_gereja
 * @property string $foto_gereja
 * @property string $sejarah_gereja
 * @property string $alamat
 * @property double $latitude
 * @property double $longitude
 * @property integer $status
 * @property string $created_by
 * @property string $updated_at
 * @property integer $kategorigereja_id
 * @property string $user_id
 *
 * @property User $user
 * @property KategoriGereja $kategorigereja
 * @property JadwalKegiatan[] $jadwalKegiatans
 */
class Gereja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gereja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_gereja', 'foto_gereja', 'sejarah_gereja', 'alamat', 'latitude', 'longitude', 'status', 'kategorigereja_id'], 'required'],
            [['sejarah_gereja', 'alamat'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['status', 'kategorigereja_id'], 'integer'],
            [['updated_at'], 'safe'],
            [['nama_gereja'], 'string', 'max' => 40],
            [['foto_gereja', 'created_by'], 'string', 'max' => 30],
            //[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['kategorigereja_id'], 'exist', 'skipOnError' => true, 'targetClass' => KategoriGereja::className(), 'targetAttribute' => ['kategorigereja_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_gereja' => 'Nama Gereja',
            'foto_gereja' => 'Foto Gereja',
            'sejarah_gereja' => 'Sejarah Gereja',
            'alamat' => 'Alamat',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'kategorigereja_id' => 'Kategorigereja ID',
            //'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKategorigereja()
    {
        return $this->hasOne(KategoriGereja::className(), ['id' => 'kategorigereja_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwalKegiatans()
    {
        return $this->hasMany(JadwalKegiatan::className(), ['gereja_id' => 'id']);
    }
}
