<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DanaSosialDetail;

/**
 * DanaSosialDetailSearch represents the model behind the search form about `app\models\DanaSosialDetail`.
 */
class DanaSosialDetailSearch extends DanaSosialDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gambar_utama', 'nomor_kontak', 'link_video', 'deskiripsi_singkat', 'deskirpsi_lengkap', 'danasosial_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DanaSosialDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'gambar_utama', $this->gambar_utama])
            ->andFilterWhere(['like', 'nomor_kontak', $this->nomor_kontak])
            ->andFilterWhere(['like', 'link_video', $this->link_video])
            ->andFilterWhere(['like', 'deskiripsi_singkat', $this->deskiripsi_singkat])
            ->andFilterWhere(['like', 'deskirpsi_lengkap', $this->deskirpsi_lengkap])
            ->andFilterWhere(['like', 'danasosial_id', $this->danasosial_id]);

        return $dataProvider;
    }
}
