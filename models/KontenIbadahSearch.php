<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KontenIbadah;

/**
 * KontenIbadahSearch represents the model behind the search form about `app\models\KontenIbadah`.
 */
class KontenIbadahSearch extends KontenIbadah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'tataibadah_id', 'jadwal_id', 'jenisibadah_id'], 'integer'],
            [['konten', 'waktu', 'link_download', 'created_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KontenIbadah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'waktu' => $this->waktu,
            'status' => $this->status,
            'tataibadah_id' => $this->tataibadah_id,
            'jadwal_id' => $this->jadwal_id,
            'jenisibadah_id' => $this->jenisibadah_id,
        ]);

        $query->andFilterWhere(['like', 'konten', $this->konten])
            ->andFilterWhere(['like', 'link_download', $this->link_download])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
}
