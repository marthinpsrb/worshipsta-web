<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kitab_isi".
 *
 * @property integer $id
 * @property string $kitab
 * @property integer $pasal
 * @property integer $ayat
 * @property string $firman
 * @property integer $versi_kitab
 *
 * @property KitabVersion $versiKitab
 */
class KitabIsi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kitab_isi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pasal', 'ayat', 'versi_kitab'], 'integer'],
            [['kitab'], 'string', 'max' => 20],
            [['firman'], 'string', 'max' => 20000],
            [['versi_kitab'], 'exist', 'skipOnError' => true, 'targetClass' => KitabVersion::className(), 'targetAttribute' => ['versi_kitab' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kitab' => 'Kitab',
            'pasal' => 'Pasal',
            'ayat' => 'Ayat',
            'firman' => 'Firman',
            'versi_kitab' => 'Versi Kitab',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersiKitab()
    {
        return $this->hasOne(KitabVersion::className(), ['id' => 'versi_kitab']);
    }
}
