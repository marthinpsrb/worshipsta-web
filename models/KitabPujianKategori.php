<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kitab_pujian_kategori".
 *
 * @property string $id
 * @property string $nama_kategori
 * @property string $keterangan
 * @property string $created_by
 * @property integer $status
 * @property string $versi_id
 *
 * @property VersiKitabPujian $versi
 * @property VersiKitabPujian[] $versiKitabPujians
 */
class KitabPujianKategori extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kitab_pujian_kategori';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_kategori', 'keterangan', 'created_by', 'status', 'versi_id'], 'required'],
            [['keterangan'], 'string'],
            [['status'], 'integer'],
            [['id', 'created_by', 'versi_id'], 'string', 'max' => 30],
            [['nama_kategori'], 'string', 'max' => 35],
            [['versi_id'], 'exist', 'skipOnError' => true, 'targetClass' => VersiKitabPujian::className(), 'targetAttribute' => ['versi_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_kategori' => 'Nama Kategori',
            'keterangan' => 'Keterangan',
            'created_by' => 'Created By',
            'status' => 'Status',
            'versi_id' => 'Versi ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersi()
    {
        return $this->hasOne(VersiKitabPujian::className(), ['id' => 'versi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersiKitabPujians()
    {
        return $this->hasMany(VersiKitabPujian::className(), ['kitabpujian_id' => 'id']);
    }
}
