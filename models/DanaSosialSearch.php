<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DanaSosial;

/**
 * DanaSosialSearch represents the model behind the search form about `app\models\DanaSosial`.
 */
class DanaSosialSearch extends DanaSosial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'target_dana', 'status_danasosial', 'status', 'kategori_id'], 'integer'],
            [['judul', 'created_by', 'deadline', 'alamat', 'user_id', 'detail_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DanaSosial::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'target_dana' => $this->target_dana,
            'deadline' => $this->deadline,
            'status_danasosial' => $this->status_danasosial,
            'status' => $this->status,
            'kategori_id' => $this->kategori_id,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'detail_id', $this->detail_id]);

        return $dataProvider;
    }
}
