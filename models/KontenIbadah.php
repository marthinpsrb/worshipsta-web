<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "konten_ibadah".
 *
 * @property integer $id
 * @property string $konten
 * @property string $waktu
 * @property string $link_download
 * @property string $created_by
 * @property integer $status
 * @property integer $tataibadah_id
 * @property integer $jadwal_id
 * @property integer $jenisibadah_id
 *
 * @property TataIbadah $tataibadah
 * @property JenisIbadah $jenisibadah
 * @property JadwalKegiatan $jadwal
 */
class KontenIbadah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'konten_ibadah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['konten', 'waktu', 'link_download', 'status', 'tataibadah_id', 'jadwal_id', 'jenisibadah_id'], 'required'],
            [['konten'], 'string'],
            [['waktu'], 'safe'],
            [['status', 'tataibadah_id', 'jadwal_id', 'jenisibadah_id'], 'integer'],
            [['link_download'], 'string', 'max' => 45],
            [['created_by'], 'string', 'max' => 30],
            [['tataibadah_id'], 'exist', 'skipOnError' => true, 'targetClass' => TataIbadah::className(), 'targetAttribute' => ['tataibadah_id' => 'id_tataibadah']],
            [['jenisibadah_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisIbadah::className(), 'targetAttribute' => ['jenisibadah_id' => 'id']],
            [['jadwal_id'], 'exist', 'skipOnError' => true, 'targetClass' => JadwalKegiatan::className(), 'targetAttribute' => ['jadwal_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'konten' => 'Konten',
            'waktu' => 'Waktu',
            'link_download' => 'Link Download',
            'created_by' => 'Created By',
            'status' => 'Status',
            'tataibadah_id' => 'Tataibadah ID',
            'jadwal_id' => 'Jadwal ID',
            'jenisibadah_id' => 'Jenisibadah ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTataibadah()
    {
        return $this->hasOne(TataIbadah::className(), ['id_tataibadah' => 'tataibadah_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisibadah()
    {
        return $this->hasOne(JenisIbadah::className(), ['id' => 'jenisibadah_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwal()
    {
        return $this->hasOne(JadwalKegiatan::className(), ['id' => 'jadwal_id']);
    }
}
