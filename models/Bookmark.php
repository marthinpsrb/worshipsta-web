<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bookmark".
 *
 * @property integer $id
 * @property string $isi_bookmark
 * @property string $created_by
 * @property string $created_at
 * @property integer $status
 * @property string $versi_id
 */
class Bookmark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookmark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isi_bookmark', 'created_by'], 'required'],
            [['isi_bookmark'], 'string'],
            [['created_at'], 'safe'],
            [['status'], 'integer'],
            [['created_by', 'versi_id'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'isi_bookmark' => 'Isi Bookmark',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'status' => 'Status',
            'versi_id' => 'Versi ID',
        ];
    }
}
