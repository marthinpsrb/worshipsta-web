<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Gereja;

/**
 * GerejaSearch represents the model behind the search form about `app\models\Gereja`.
 */
class GerejaSearch extends Gereja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'kategorigereja_id'], 'integer'],
            [['nama_gereja', 'foto_gereja', 'sejarah_gereja', 'alamat', 'created_by', 'updated_at', 'user_id'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gereja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'kategorigereja_id' => $this->kategorigereja_id,
        ]);

        $query->andFilterWhere(['like', 'nama_gereja', $this->nama_gereja])
            ->andFilterWhere(['like', 'foto_gereja', $this->foto_gereja])
            ->andFilterWhere(['like', 'sejarah_gereja', $this->sejarah_gereja])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);
            //->andFilterWhere(['like', 'user_id', $this->user_id]);

        return $dataProvider;
    }
}
