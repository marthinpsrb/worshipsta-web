<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kidung".
 *
 * @property integer $id
 * @property integer $nomor
 * @property string $judul
 * @property integer $versi_kidung
 *
 * @property KidungVersion $versiKidung
 */
class Kidung extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kidung';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nomor', 'judul', 'versi_kidung'], 'required'],
            [['id', 'nomor', 'versi_kidung'], 'integer'],
            [['judul'], 'string', 'max' => 255],
            [['versi_kidung'], 'exist', 'skipOnError' => true, 'targetClass' => KidungVersion::className(), 'targetAttribute' => ['versi_kidung' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor' => 'Nomor',
            'judul' => 'Judul',
            'versi_kidung' => 'Versi Kidung',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersiKidung()
    {
        return $this->hasOne(KidungVersion::className(), ['id' => 'versi_kidung']);
    }
}
