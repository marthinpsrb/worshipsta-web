<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property string $nama_lengkap
 * @property string $nomor_identitas
 * @property string $jemaat_gereja
 * @property string $foto_gereja
 * @property string $created_by
 * @property string $updated_at
 * @property string $user_id
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_lengkap', 'nomor_identitas', 'jemaat_gereja', 'foto_gereja', 'created_by', 'updated_at', 'user_id'], 'required'],
            [['updated_at'], 'safe'],
            [['nama_lengkap'], 'string', 'max' => 45],
            [['nomor_identitas', 'jemaat_gereja', 'foto_gereja', 'created_by', 'user_id'], 'string', 'max' => 30],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_lengkap' => 'Nama Lengkap',
            'nomor_identitas' => 'Nomor Identitas',
            'jemaat_gereja' => 'Jemaat Gereja',
            'foto_gereja' => 'Foto Gereja',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
