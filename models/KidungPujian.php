<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kidung_pujian".
 *
 * @property string $id
 * @property integer $nomor
 * @property integer $nomor_notbalok
 * @property integer $ayat
 * @property string $lagu
 * @property string $versi_id
 *
 * @property VersiKitabPujian $versi
 */
class KidungPujian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kidung_pujian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nomor', 'nomor_notbalok', 'ayat', 'lagu', 'versi_id'], 'required'],
            [['nomor', 'nomor_notbalok', 'ayat'], 'integer'],
            [['lagu'], 'string'],
            [['id', 'versi_id'], 'string', 'max' => 30],
            [['versi_id'], 'exist', 'skipOnError' => true, 'targetClass' => VersiKitabPujian::className(), 'targetAttribute' => ['versi_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor' => 'Nomor',
            'nomor_notbalok' => 'Nomor Notbalok',
            'ayat' => 'Ayat',
            'lagu' => 'Lagu',
            'versi_id' => 'Versi ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVersi()
    {
        return $this->hasOne(VersiKitabPujian::className(), ['id' => 'versi_id']);
    }
}
