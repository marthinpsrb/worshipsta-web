<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dana_sosial_detail".
 *
 * @property string $id
 * @property string $gambar_utama
 * @property string $nomor_kontak
 * @property string $link_video
 * @property string $deskiripsi_singkat
 * @property string $deskirpsi_lengkap
 * @property string $danasosial_id
 *
 * @property DanaSosial[] $danaSosials
 * @property DanaSosialDetail $danasosial
 * @property DanaSosialDetail[] $danaSosialDetails
 */
class DanaSosialDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dana_sosial_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gambar_utama', 'nomor_kontak', 'link_video', 'deskiripsi_singkat', 'deskirpsi_lengkap', 'danasosial_id'], 'required'],
            [['deskiripsi_singkat', 'deskirpsi_lengkap'], 'string'],
            [['id', 'gambar_utama', 'danasosial_id'], 'string', 'max' => 30],
            [['nomor_kontak'], 'string', 'max' => 13],
            [['link_video'], 'string', 'max' => 80],
            [['danasosial_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanaSosialDetail::className(), 'targetAttribute' => ['danasosial_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gambar_utama' => 'Gambar Utama',
            'nomor_kontak' => 'Nomor Kontak',
            'link_video' => 'Link Video',
            'deskiripsi_singkat' => 'Deskiripsi Singkat',
            'deskirpsi_lengkap' => 'Deskirpsi Lengkap',
            'danasosial_id' => 'Danasosial ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDanaSosials()
    {
        return $this->hasMany(DanaSosial::className(), ['detail_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDanasosial()
    {
        return $this->hasOne(DanaSosialDetail::className(), ['id' => 'danasosial_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDanaSosialDetails()
    {
        return $this->hasMany(DanaSosialDetail::className(), ['danasosial_id' => 'id']);
    }
}
