<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KidungPujian;

/**
 * KidungPujianSearch represents the model behind the search form about `app\models\KidungPujian`.
 */
class KidungPujianSearch extends KidungPujian
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lagu', 'versi_id'], 'safe'],
            [['nomor', 'nomor_notbalok', 'ayat'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KidungPujian::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nomor' => $this->nomor,
            'nomor_notbalok' => $this->nomor_notbalok,
            'ayat' => $this->ayat,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'lagu', $this->lagu])
            ->andFilterWhere(['like', 'versi_id', $this->versi_id]);

        return $dataProvider;
    }
}
