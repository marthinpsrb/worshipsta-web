<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kitab;

/**
 * KitabSearch represents the model behind the search form about `app\models\Kitab`.
 */
class KitabSearch extends Kitab
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'versi_kitab'], 'integer'],
            [['kitab', 'kategori_kitab'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kitab::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'versi_kitab' => $this->versi_kitab,
        ]);

        $query->andFilterWhere(['like', 'kitab', $this->kitab])
            ->andFilterWhere(['like', 'kategori_kitab', $this->kategori_kitab]);

        return $dataProvider;
    }
}
