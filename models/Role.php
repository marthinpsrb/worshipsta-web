<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $nama_role
 * @property string $keterangan
 * @property integer $status
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_role', 'keterangan', 'status'], 'required'],
            [['keterangan'], 'string'],
            [['status'], 'integer'],
            [['nama_role'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_role' => 'Nama Role',
            'keterangan' => 'Keterangan',
            'status' => 'Status',
        ];
    }
}
