<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dana_sosial".
 *
 * @property integer $id
 * @property string $judul
 * @property string $created_by
 * @property integer $target_dana
 * @property string $deadline
 * @property string $alamat
 * @property integer $status_danasosial
 * @property integer $status
 * @property integer $kategori_id
 * @property string $user_id
 * @property string $detail_id
 *
 * @property User $user
 * @property DanaSosial $kategori
 * @property DanaSosial[] $danaSosials
 * @property DanaSosialDetail[] $danaSosialDetails
 */
class DanaSosial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dana_sosial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'created_by', 'target_dana', 'deadline', 'alamat', 'status_danasosial', 'status', 'kategori_id', 'user_id', 'detail_id'], 'required'],
            [['target_dana', 'status_danasosial', 'status', 'kategori_id'], 'integer'],
            [['deadline'], 'safe'],
            [['alamat'], 'string'],
            [['judul'], 'string', 'max' => 35],
            [['created_by', 'user_id', 'detail_id'], 'string', 'max' => 30],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['kategori_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanaSosial::className(), 'targetAttribute' => ['kategori_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'created_by' => 'Created By',
            'target_dana' => 'Target Dana',
            'deadline' => 'Deadline',
            'alamat' => 'Alamat',
            'status_danasosial' => 'Status Danasosial',
            'status' => 'Status',
            'kategori_id' => 'Kategori ID',
            'user_id' => 'User ID',
            'detail_id' => 'Detail ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKategori()
    {
        return $this->hasOne(DanaSosial::className(), ['id' => 'kategori_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDanaSosials()
    {
        return $this->hasMany(DanaSosial::className(), ['kategori_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDanaSosialDetails()
    {
        return $this->hasMany(DanaSosialDetail::className(), ['danasosial_id' => 'id']);
    }
}
